import Tag from '@/models/Tag'

test('Tag.create() creates a Tag based on the text given', () => {
    var tag1 = Tag.create(Tag.C_SHARP)
    expect(tag1.getText()).toBe('C#')
    expect(tag1.getIcon()).toBe('c-sharp')
})

test('Tag.create() returns null for a tag without an icon', () => {
    var tag2 = Tag.create(Tag.C_SHARP + '~')
    expect(tag2).toBe(null)
})