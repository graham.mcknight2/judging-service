import Model from '@/models/Model'
import MockedBackendManager from '@/interop/MockedBackendManager'
import CachedBackendManager from '@/interop/CachedBackendManager'

const TestModel = function(
    id,
    fieldA = '') {
    Model.call(this, id)

    this._fieldA = fieldA
}

const models = [
    new TestModel(1, 'a'),
    new TestModel(2, 'b'),
    new TestModel(3, 'c')]

test('mocked backend can initialize with stored values', async () => { 
    var manager = new MockedBackendManager(models, [])

    var model = await manager.loadModel(1, 'TestModel')
    expect(model.id).toBe(1)
    expect(model.fieldA).toBe('a')

    model = await manager.loadModel(2, 'TestModel')
    expect(model.id).toBe(2)
    expect(model.fieldA).toBe('b')
})

test('mocked backend honors updated values', async () => {
    var manager = new MockedBackendManager(models, [])

    var model = await manager.loadModel(1, 'TestModel')
    expect(model.id).toBe(1)
    expect(model.fieldA).toBe('a')

    await manager.pushModel(new TestModel(1, 'e'))
    model = await manager.loadModel(1, 'TestModel')
    expect(model.id).toBe(1)
    expect(model.fieldA).toBe('e')
})

test('cached backend stores models', async() => {
    var innerManager = new MockedBackendManager(models, [])
    var cachedManager = new CachedBackendManager(innerManager)

    var model = await cachedManager.loadModel(1)
    expect(model.id).toBe(1)
    expect(model.fieldA).toBe('a')

    // Wipe out the back end's data to make sure we don't hit it
    innerManager._models = {}

    model = await cachedManager.loadModel(1)
    expect(model.id).toBe(1)
    expect(model.fieldA).toBe('a')

    // Sanity check -- doesn't load a model which it hasn't cached yet
    model = await cachedManager.loadModel(2)
    expect(model).toBe(undefined)
})