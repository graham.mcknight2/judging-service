import Model from '@/models/Model'
import MockedBackendManager from '@/interop/MockedBackendManager'
import backend from '@/backend-manager'

const TestModel = function(id) {
    Model.call(this, id)

    this._fieldA = ''
    this._fieldB = ''
    this._fieldC = ''

    this.setFieldB = function(value) {
        this._fieldC = value
    }
}

test('model correctly loads from object', () => {
    var model = new TestModel('test_id')
    expect(model._id).toBe('test_id')

    model._addInformation({fieldA: 'test_A', fieldB: 'test_B'})
    expect(model._fieldA).toBe('test_A');
    expect(model._fieldB).toBe('');
    expect(model._fieldC).toBe('test_B');
})

test('mocked backend is useable by model', async() => {
    var modelA = new TestModel(1)
    modelA._fieldA = 'a'
    backend.setInstance(new MockedBackendManager([ modelA ], []))
    
    var modelB = new TestModel(1)
    await modelB.load()
    expect(modelB._fieldA).toBe('a')
})