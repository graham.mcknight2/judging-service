import Vue from 'vue'
import Router from 'vue-router'

import LoginPage from '@/views/LoginPage'
import ContestView from '@/views/ContestView'
import CompetitionCreationView from '@/views/CompetitionCreationView'
import ContestBrowser from '@/views/ContestBrowser'
import SubmissionCreationView from '@/views/SubmissionCreationView'
import SubmissionBrowser from '@/views/SubmissionBrowser'
import SubmissionView from '@/views/SubmissionView'
import UserCreationPage from '@/views/UserCreationPage'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/Login',
            name: 'LoginPage',
            component: LoginPage
        },
        {
            path: '/UserCreationPage',
            name: 'UserCreationPage',
            component: UserCreationPage
        },
        {
            path: '/ContestView/:id',
            name: 'ContestView',
            component: ContestView
        },
        {
            path: '/CompetitionCreationView',
            name: 'CompetitionCreationView',
            component: CompetitionCreationView
        },
        {
            path: '/Contests',
            name: 'Contests',
            component: ContestBrowser
        },
        {
            path: '/Submit/:id',
            name: 'SubmissionCreationView',
            component: SubmissionCreationView
        },
        {
            path: '/Submissions',
            name: 'SubmissionBrowser',
            component: SubmissionBrowser
        },
        {
            path: '/Submissions/:id',
            name: 'SubmissionResultView',
            component: SubmissionView
        }
    ]
})
