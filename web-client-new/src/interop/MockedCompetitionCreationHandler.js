import HttpCompetitionCreationHandler from '@/interop/HttpCompetitionCreationHandler'

export default function MockedCompetitionCreationHandler(backend, uploadFiles, sasUri) {
    HttpCompetitionCreationHandler.call(this, backend)

    this._sasUri = sasUri

    if (!uploadFiles) {
        this._uploadFile = async function () {}
    }

    this.registerStarterFiles = async function (starterFiles) {
        if (starterFiles.length != 1) {
            throw 'Need exactly one file!'
        }

        await this._uploadFile(starterFiles[0], this._sasUri)
    }

    this.registerJudgingFiles = async function(judgingFiles) {
        if (judgingFiles.length != 1) {
            throw 'Need exactly one file!'
        }

        await this._uploadFile(judgingFiles[0], this._sasUri)
    }
}