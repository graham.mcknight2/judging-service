import Model from '@/models/Model'
import BaseBackendManager from '@/interop/BaseBackendManager'

/**
 * A decorator for a backend manager that caches results of
 * queries to avoid repeats.
 * 
 * @param {BaseBackendManager} backendManager 
 */
export default function CachedBackendManager(backendManager) {
    BaseBackendManager.call(this)
    
    this._backendManager = backendManager
    this._modelsCache = {}

    /**
     * Clear the cache so that the models can be updated.
     * Caching should be observed for immutable or unlikely to
     * change objects.
     */
    this.clear = function() {
        this._modelsCache = {}
    }

    this.loadModel = async function(id, modelType) {
        if (id in this._modelsCache) {
            return this._modelsCache[id]
        }
        
        var result = await this._backendManager.loadModel(id, modelType)
        if (result) {
            this._modelsCache[result.id] = result
        }
        return result
    }

    /**
     * Function that will put or post some information from a model on the
     * competition service, for an update or creation.
     */
    this.pushModel = async function (model) {
        if (model._id in this._modelsCache) {
            delete this._modelsCache[model._id]
        }
        await this._backendManager.pushModel(model)
    }

    this.getSubmissionCreationManager = function (submissionId) {
        return this._backendManager.getSubmissionCreationManager(submissionId)
    }
    this.getCompetitionCreationManager = function (competitionId) {
        return this._backendManager.getCompetitionCreationManager(competitionId)
    }

    
    this.authenticateWithServer = async function (username, password) {
        return await this._backendManager.authenticateWithServer(username, password)
    }
    this.createAccount = async function (username, password) {
        return await this._backendManager.createAccount(username, password)
    }
    this.getUpcomingCompetitionList = async function () {
        return await this._backendManager.getUpcomingCompetitionList()
    }
    this.getSubmissionListForUser = async function (id) {
        return await this._backendManager.getSubmissionListForUser()
    }
}