import Competition from '@/models/Competition'

export default function BaseCompetitionCreationHandler(backend) {
    this._competition = {}
    this._backend = backend

    /**
     * Handles the workflow for submitting a competition to the backend.
     * 
     * @param {*} competition The model for the competition that has been
     * submitted by the user.
     */
    this.submit = async function(competition) {
        this._competition = competition
        await backend.pushModel(competition)
        await competition.load()
    }

    /* A set of methods to be overriden by the subclasses */
    this.registerStarterFiles = async function(starterFiles) {}
    this.registerJudgingFiles = async function(judgingFiles) {}
    
}