import BaseCompetitionCreationHandler from "@/interop/BaseCompetitionCreationHandler";
import BaseSubmissionCreationHandler from "@/interop/BaseSubmissionCreationHandler";

export default function BaseBackendManager() {

    this.loadModel = async function(id, modelType) {
        return {}
    }

    /**
     * Function that will put or post some information from a model on the
     * competition service, for an update or creation.
     */
    this.pushModel = async function (model) {}

    this.getSubmissionCreationManager = function (submissionId) {
        return new BaseSubmissionCreationHandler(this)
    }
    this.getCompetitionCreationManager = function (competitionId) {
        return new BaseCompetitionCreationHandler(this)
    }

    /**
     * One-off interactions the backend managers must implement
     * that do not fit consistently with the rest of the framework.
     */
    this.authenticateWithServer = async function (username, password) {}
    this.createAccount = async function (username, password) {}
    this.getUpcomingCompetitionList = async function () { return [] }
    this.getSubmissionListForUser = async function (id) { return [] }
}