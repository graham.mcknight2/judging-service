export default function BaseSubmissionCreationHandler(backend) {
    this._submission = {}
    this._backend = backend

    /**
     * Handles the workflow for submitting a submission to the backend.
     * 
     * @param {*} submission The model for the submission that has been
     * submitted by the user.
     */
    this.submit = async function(submission) {
        this._submission = submission
        await backend.pushModel(submission)
        await submission.load()
    }

    this.registerFiles = async function(files) {}
    this.finish = async function() {}
    
}