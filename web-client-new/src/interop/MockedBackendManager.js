import BaseBackendManager from '@/interop/BaseBackendManager'
import BaseCompetitionCreationHandler from "@/interop/BaseCompetitionCreationHandler"
import BaseSubmissionCreationHandler from "@/interop/BaseSubmissionCreationHandler"
import MockedCompetitionCreationHandler from '@/interop/MockedCompetitionCreationHandler'
import MockedSubmissionCreationHandler from '@/interop/MockedSubmissionCreationHandler'

import Submission from '@/models/Submission'
import Competition from '@/models/Competition'

export default function MockedBackendManager(models, users) {

    BaseBackendManager.call(this)

    this._randInt = function() {
        return Math.floor(Math.random() * 9999);
    }

    /**
     * Unpacks all fields from a model such as _id to a new object where
     * the fields are named without the leading underscore.
     * @param {*} model 
     */
    this._unpackPrivateFields = function(model) {
        var toStore = {}
        for (var key in model) {
            if (key.charAt(0) == '_') {
                toStore[key.substr(1)] = model[key]
            }
        }
        toStore._mockMeta = { type: model.getModelType() }
        return toStore
    }

    /**
     * Extracts a set of fields on one field that they have
     * @param {*} objects 
     * @param {*} keyField 
     * @param {boolean} generateKey
     * @param {boolean} unpackPrivate
     */
    this._mapObjects = function(objects, keyField, generateKey = false, unpackPrivate = true) {
        var map = {}
        for (var i = 0; i < objects.length; i++) {
            if (generateKey && !objects[i][keyField]) {
                objects[i][keyField] = this._randInt()
            }

            if (unpackPrivate) {
                map[objects[i][keyField]] = this._unpackPrivateFields(objects[i])
            } else {
                map[objects[i][keyField]] = models[i]
            }

        }
        return map
    }

    this._models = this._mapObjects(models, '_id', true)
    this._users = this._mapObjects(users, 'username')

    this.loadModel = async function(id, modelType) {
        return this._models[id]
    }

    /**
     * Function that will put or post some information from a model on the
     * competition service, for an update or creation.
     */
    this.pushModel = async function (model) {
        if (!model._id) {
            model.assignId(this._randInt())
        }

        this._models[model._id] = this._unpackPrivateFields(model)
    }

    this.getSubmissionCreationManager = function (submissionId) {
        return new MockedSubmissionCreationHandler(this, false, '')
    }
    this.getCompetitionCreationManager = function (competitionId) {
        return new MockedCompetitionCreationHandler(this, false, 
            'http://127.0.0.1:10000/devstoreaccount1/contests/3692918e-2157-4310-a6ee-754214890491?sv=2018-03-28&sr=b&sig=qy19L4ysUxuri64QE3YQuqh26dAeYU%2Buul%2FVZcCejcY%3D&se=2021-10-31T03%3A13%3A16Z&sp=cw')
    }

    /**
     * One-off interactions the backend managers must implement
     * that do not fit consistently with the rest of the framework.
     */
    this.authenticateWithServer = async function (username, password) {}
    this.createAccount = async function (username, password) {}
    this.getUpcomingCompetitionList = async function () {
        var upcoming = []
        var now = new Date()
        var data
        var model
        for (var key in this._models) {
            data = this._models[key]
            if (data._mockMeta.type == "Competition") {
                model = new Competition(data.id)
                model.load()
                upcoming.push(model)
            }
        }
        return upcoming
    }
    
    this.getSubmissionListForUser = async function (id) {
        // TODO: Base this on the user that was authenticated
        var submissions = []
        var data
        var model
        for (var key in this._models) {
            data = this._models[key]
            if (data._mockMeta.type == "Submission") {
                model = new Submission(data.id)
                model.load()
                submissions.push(model)
            }
        }
        return submissions
    }
}