import axios from 'axios'
import BaseCompetitionCreationHandler from "./BaseCompetitionCreationHandler"

export default function HttpCompetitionCreationHandler(backend) {
    BaseCompetitionCreationHandler.call(this, backend)
    this._config = {
        headers: {
            'x-ms-blob-type': 'BlockBlob',
        }
    }

    this._uploadFile = async function(file, url) {
        // TODO: Use chunking if the file is excessively large
        console.log('Starting to upload to url: ' + url)
        await axios.put(url, file, this._config)
        console.log('Done uploading')
    }

    this.registerStarterFiles = async function(starterFiles) {
        // TODO
    }

    this.registerJudgingFiles = async function(judgingFiles) {
        if (judgingFiles.length != 1) {
            throw 'Need exactly one file!'
        }

        // TODO: Register each file with the backend, once it
        // has the ability.
        var response = await axios.get(
            this._backend._baseCompetitionServiceUrl + 'Competition/' + this._competition._id + '/script',
            this._backend._config);
        var sasUri = response.data

        console.log('Got the following uri: ' + sasUri)

        await this._uploadFile(judgingFiles[0], sasUri)
    }
}