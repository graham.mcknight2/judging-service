import axios from 'axios'
import BaseBackendManager from "@/interop/BaseBackendManager"
import HttpCompetitionCreationHandler from '@/interop/HttpCompetitionCreationHandler'
import HttpSubmissionCreationHandler from '@/interop/HttpSubmissionCreationHandler'
import Competition from '@/models/Competition'
import Submission from '@/models/Submission'


/**
 * 
 * @param {*} baseAuthenticationServiceUrl 
 * @param {*} baseCompetitionServiceUrl 
 * @param {*} baseResultServiceUrl 
 */
export default function HttpBackendManager(baseAuthenticationServiceUrl, 
    baseCompetitionServiceUrl, baseResultServiceUrl) {
    
    BaseBackendManager.call(this)
    
    this._baseCompetitionServiceUrl = baseCompetitionServiceUrl

    // TODO: Move to state so it can be changed when logging out
    this._authenticationToken = ''
    this._config = {
        headers: {
            'Content-Type': 'application/json',
            'x-ms-blob-type': 'BlockBlob',
            Token: ''
        }
    }
    
    // TODO: relocate to a util file
    this._goodStatus =  function (response) {
        return response.status >= 200 && response.status < 300
    },

    /**
     * Loads the model by changing the uri path params, assumes
     * a certain structure
     * @param {*} id 
     * @param {*} modelType 
     */
    this.loadModel = async function(id, modelType) {
        var response = await axios.get(
            this._baseCompetitionServiceUrl + modelType + '/' + id,
            this._config)

        console.log(response)

        if (this._goodStatus(response)) {
            return response.data
        }
    }

    /**
     * Creates, doesn't currently handle the already existing case for
     * a model. Assumes that the endpoint returns the model that was
     * just created, with any changes.
     * 
     * @param {*} model 
     * 
     * TODO: Make a put if the model has an id of 0
     */
    this.pushModel = async function(model) {
        console.log('Pushing model: ' + JSON.stringify(model) + ' of type ' + model.getModelType())
        
        var response = await axios.post(this._baseCompetitionServiceUrl + model.getModelType(),
                model.toJSON(), this._config)

        if (this._goodStatus(response)) {
            console.log('Setting the model id to ' + response.data.id)
            model.assignId(response.data.id)
            return response.data.id
        }
    }

    this.getSubmissionCreationManager = function (submissionId) {
        return new HttpSubmissionCreationHandler(this)
    }
    this.getCompetitionCreationManager = function (competitionId) {
        return new HttpCompetitionCreationHandler(this)
    }

    /* One-off methods */
    this.authenticateWithServer = async function (username, password) {
        // TODO: Remove
        //await backendManager.authenticateWithServer(username, password)

        try {
            var response = await axios.post(this._baseCompetitionServiceUrl + 'Login',
                { email: username, password: password }, this._config)

            if (this._goodStatus(response)) {
                this._config.headers.Token = response.data
                this._authenticationToken = response.data
            }

            console.log(response)
            return this._goodStatus(response)
        } catch (e) {
            console.log(e)
            return false
        }
    }

    this.createAccount = async function (username, password) {
        var response = await axios.post(this._baseCompetitionServiceUrl + 'Login/new',
                { email: username, password: password }, this._config)

        console.log(response)
        return this._goodStatus(response)
    }

    this.getUpcomingCompetitionList = async function () {
        var response = await axios.get(
                this._baseCompetitionServiceUrl + 'Competition/upcoming',
                this._config)
    
        console.log(response)
        if (!this._goodStatus(response)) {
            return
        }

        var models = []
        for (var i = 0; i < response.data.length; i++) {
            var compData = response.data[i]
            console.log('CompData: ' + compData)
            var competition = new Competition(compData.id)
            competition.load(compData)
            models.push(competition)
        }

        return models
    }

    this.getSubmissionListForUser = async function () {
        var response = await axios.get(
            this._baseCompetitionServiceUrl + 'Submission/user/',
            this._config)

        console.log(response)

        if (!this._goodStatus(response)) {
            return []
        }

        var models = []
        for (var i = 0; i < response.data.length; i++) {
            var subData = response.data[i]
            var submission = new Submission(subData.id)
            submission.load(subData)
            models.push(submission)
        }

        return models
    }
}