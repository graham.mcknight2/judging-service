import HttpSubmissionCreationHandler from '@/interop/HttpSubmissionCreationHandler'

export default function MockedSubmissionCreationHandler(backend, uploadFiles, sasUri) {
    HttpSubmissionCreationHandler.call(this, backend)

    this._sasUri = sasUri

    if (!uploadFiles) {
        this._uploadFile = async function () {}
    }

    this.registerFiles = async function(files) {
        if (files.length != 1) {
            throw 'Need exactly one file!'
        }

        await this._uploadFile(files[0], this._sasUri)
    }

}