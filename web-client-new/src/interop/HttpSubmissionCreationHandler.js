import axios from 'axios'
import BaseSubmissionCreationHandler from "./BaseSubmissionCreationHandler"

export default function HttpSubmissionCreationHandler(backend) {
    BaseSubmissionCreationHandler.call(this, backend)
    this._config = {
        headers: {
            'x-ms-blob-type': 'BlockBlob',
        }
    }

    this._uploadFile = async function(file, url) {
        // TODO: Use chunking if the file is excessively large
        await axios.put(url, file, this._config)
    }

    this.registerFiles = async function(files) {
        if (files.length != 1) {
            throw 'Need exactly one file!'
        }

        // TODO: Register each file with the backend, once it
        // has the ability.
        var response = await axios.get(
            this._backend._baseCompetitionServiceUrl + 'Submission/' + this._submission._id + '/token',
            this._backend._config)
        var sasUri = response.data

        console.log('Got the following uri: ' + sasUri)

        await this._uploadFile(files[0], sasUri)
    }

    this.finish = async function() {
        await axios.put(
            this._backend._baseCompetitionServiceUrl + 'Submission/' + this._submission._id,
            {}, // Data
            this._backend._config)
    }
}