import backend from '@/backend-manager'

/**
 * Defines the type for a model which will take information from
 * the competition service.
 */
export default function Model(id) {
    // All models should be loadable given only an id
    this._id = id
    
    // Allows properties to be mapped to different names from those that
    // come raw from the source
    this._transformations = {}

    /**
     * Method to be overriden to veto the setting of a property based
     * on its source.
     * 
     * @param {string} name The name of the property to set in the 
     */
    // eslint-disable-next-line
    this._canSetProperty = function(name) {
        if (name == 'id') {
            return false
        }
        return true
    }

    /**
     * Give an id to an existing model that didn't previously have a model
     * once it is created on the server.
     * @param {*} id 
     */
    this.assignId = function(id) {
        if (!this._id) {
            this._id = id
        }
    }

    /**
     * Load information the competition service and set the properties of this model
     * accordingly.
     */
    this.load = async function (values) {
        if (!values)
            var values = await backend.getInstance().loadModel(this._id, this.getModelType())
        this._addInformation(values)
    }

    /**
     * Takes an object returned from a request and attempts to set the model's
     * corresponding properties.
     * 
     * @param {Object} values The body of the result from which we set the properties
     * in our own model
     */
    this._addInformation = function(values) {
        for (var valuesKey in values) {
            var modelKey = this._transformations[valuesKey] || valuesKey

            if (!this._canSetProperty(modelKey)) {
                console.log('Property ' + modelKey + ' blocked from set; value ' + values[valuesKey])
                continue
            }

            // If we have a setter for the property, we will invoke that function
            // with the value that the body gives
            var setterName = 'set' + modelKey.charAt(0).toUpperCase() + modelKey.substr(1)
            if (typeof this[setterName] === 'function') {
                this[setterName](values[valuesKey])
                continue
            }

            // If we have a property under the name, we will set it directly
            if (this.hasOwnProperty('_' + modelKey)) {
                this['_' + modelKey] = values[valuesKey]
                continue
            }

            // Ignore extraneous values
            console.log('Property ' + modelKey + ' not set; value ' + values[valuesKey])
        }
    }

    /**
     * Convert this model into a JSON format that can be used
     * by the backend.
     */
    this.toJSON = function() {
        var jobj = {}
        for (var key in this) {
            // We should not push values that are null, empty, or
            // undefined on our object because this might include
            // guids. Instead, we let the default values of the
            // models on the backend make the decision
            if (key.charAt(0) == '_' && this[key]) {
                jobj[key.substr(1)] = this[key]
            }
        }
        return jobj
    }

    /**
     * Hardcoding the type of the model to get around minimization
     */
    this.getModelType = function() {
        return 'Model'
    }
}