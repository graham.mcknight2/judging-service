export default function Tag(icon, text) {
    this._icon = icon
    this._text = text
    
    this.getIcon = function() {
        return this._icon
    }
    this.getText = function() {
        return this._text
    }

    this.isValid = function() {
        return Tag._textIconMapping[this._text] == this._icon
    }

    this.getModelType = function() {
        return 'Tag'
    }
}

// Class members
Tag.C_SHARP = 'C#'
Tag.PYTHON = 'Python'
Tag.JAVASCRIPT = 'Javascript'
Tag.C = 'C'
Tag.C_PLUS_PLUS = 'C++'
Tag.TOURNAMENT = 'Against Others'
Tag.HOT = 'Hot'
Tag.NEW = 'New'

Tag._textIconMapping = {
    'C#': 'c-sharp',
    'Python': 'python',
    'Javascript': 'javascript',
    'C': 'c',
    'C++': 'c-plus-plus',
    'Against Others': 'tournament',
    'Hot': 'fire',
    'New': 'new'  
}

/**
 * Creates a tag given the text for it.
 */
Tag.create = function(text) {
    // Give nothing back if we don't have a real icon.
    if (!Tag._textIconMapping[text]) {
        return null
    }

    return new Tag(Tag._textIconMapping[text], text)
}