import Model from "@/models/Model"

/**
 * Model for a submission made by a user. Loadable from the id on the
 * backend.
 * @param {string} id 
 * @param {string} userId 
 * @param {string} competitionId 
 * @param {string} status 
 * @param {Date} submitted 
 * @param {Object} results 
 * @param {number} rank 
 * @param {number} worstRank
 * @param {number} score
 */
export default function Submission(
    id,
    userId = '',
    competitionId = '',
    status = '',
    submitted = new Date(0),
    tests = {},
    rank = 0,
    worstRank = 0,
    score = 0) {

    Model.call(this, id)

    this._userId = userId
    this._competitionId = competitionId
    this._status = status
    this._submitted = submitted
    this._tests = tests
    this._rank = rank
    this._worstRank = worstRank
    this._score = score

    this.getUserId = function() {
        return this._userId
    }
    this.getCompetitionId = function() {
        return this._competitionId
    }
    this.getStatus = function () {
        return this._status
    }
    this.getSubmitted = function() {
        return this._submitted
    }
    this.getTests = function() {
        return this._tests
    }
    this.getRank = function() {
        return this._rank
    }
    this.getWorstRank = function() {
        return this._worstRank
    }
    this.getScore = function() {
        return this._score
    }

    this.getModelType = function() {
        return 'Submission'
    }
}