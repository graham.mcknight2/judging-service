import Model from "@/models/Model"

/**
 * The competition class holds a partial or complete set of information
 * that may need to be retrieved from the competition and result services.
 * 
 * @param {string} id The guid associated with this competition on the backend
 * @param {string} title 
 * @param {Date} deadline 
 * @param {number} submissions 
 * @param {Array<Tag>} tags 
 * @param {string} embedUrl The page for additional description that gets loaded
 * when viewing a competition
 * @param {string} description 
 * @param {string} imageUrl 
 */
export default function Competition(
    id,
    title='',
    deadline = new Date(0),
    submissions = 0,
    tags = [],
    embedUrl = '',
    description = '',
    imageUrl = '') {
    
    Model.call(this, id)

    this._title = title
    this._deadline = deadline
    this._submissions = submissions
    this._tags = tags
    this._embedUrl = embedUrl
    this._description = description
    this._imageUrl = imageUrl

    this.setDeadline = function(deadline) {
        if (deadline instanceof Date) {
            this._deadline = deadline
            return
        }

        this._deadline = new Date(deadline)
    }

    this.setTags = function(tags) {} // TODO do not currently unpack tags

    this.getTitle = function() { return this._title }
    this.getId = function() { return this._id }
    this.getDeadline = function() { return this._deadline }
    this.getSubmissions = function() { return this._submissions }
    this.getEmbedUrl = function() { return this._embedUrl }
    this.getDescription = function() { return this._description }
    this.getImageUrl = function() { return this._imageUrl }
    
    /**
     * Getter for the tags may include adding some tags if
     * applicable.
     */
    this.getTags = function() {
        return this._tags
    }

    this.getModelType = function() {
        return 'Competition'
    }
}
