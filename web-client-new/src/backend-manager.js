import axios from 'axios'
import HttpBackendManager from '@/interop/HttpBackendManager';

var backendManager = {
    // TODO: Load from config, find a better place for these
    baseAuthenticationServiceUrl: 'http://localhost:51817/api/',
    baseCompetitionServiceUrl: '/api/',
    baseAzureFunctionUrl: 'http://localhost:7071/api/',

    /**
     * The new backend manager that can be swapped out for
     * when creating a mocked environment.
     */
    _manager: new HttpBackendManager(
        'http://localhost:51817/api/',
        '/api/',
        'http://localhost:7071/api/'),

    getInstance: function() {
        return this._manager
    },

    setInstance: function(newManager) {
        this._manager = newManager
    }
}

export default backendManager