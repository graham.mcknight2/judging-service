# https://docs.docker.com/engine/examples/dotnetcore/#prerequisites
FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY CompetitionService/ ./
RUN dotnet restore
RUN dotnet publish CompetitionService -c Release -o out