import xml.etree.ElementTree as ElementTree

# dotnet test Tests "-l:trx;LogFileName=../../../TestOutput.xml"

tree = ElementTree.parse('TestOutput.xml')
root = tree.getroot()

for child in root:
    if ('Results' in child.tag):
        for test in child:
            print(test.attrib['outcome'].upper(), '\t--> ', test.attrib['testName'])