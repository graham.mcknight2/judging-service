# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=judging-service:build-env /app/JudgingService/out .
ENTRYPOINT ["dotnet", "JudgingService.dll"]