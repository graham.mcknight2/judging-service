import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.*;

public class Grader {
    public static void main(String[] args) {
        int[] submissionArray = ArrayMaker.makeArray(10);
        JsonObject results = new JsonObject();
        if (submissionArray == null) {
            results.addProperty("score", 0);
            results.addProperty("explaination", "ArrayMaker.makeArray(10) returned a null object");
        } else {
            results.addProperty("length", submissionArray.length);
            int correct = 0;
            for (int i = 0; i < Math.min(submissionArray.length, 10); i++) {
                if (submissionArray[i] == i) {
                    correct++;
                }
            }
            results.addProperty("score", correct);
        }
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter("result.json");
            gson.toJson(results, writer);
            writer.close();
        } catch (IOException ignored) {}
    }
}