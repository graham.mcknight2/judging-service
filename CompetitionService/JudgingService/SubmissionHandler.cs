﻿using AzureServices;
using Commons.Models;
using JudgingService.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading.Tasks;

namespace JudgingService
{
    public class SubmissionHandler
    {
        private static readonly string Workspace = "workspace";
        private static readonly string Competition = "contest";
        private static readonly string Submission = "submission";

        private ICompetitionDatabase database;
        private IAzureStorageClient storage;
        private string homeDirectory;
        public bool ShouldSleep { get; private set; }

        public SubmissionHandler(ICompetitionDatabase database, IAzureStorageClient storage, string homeDirectory)
        {
            this.database = database;
            this.storage = storage;
            this.homeDirectory = homeDirectory;
        }

        public async Task HandleNextMessage()
        {
            Console.WriteLine("Getting next message");
            SubmissionMessage message = await storage.NextMessage();
            if (message == null)
            {
                Console.WriteLine("No message on queue, sleeping");
                ShouldSleep = true;
                return;
            }

            purgeWorkspace();

            Console.WriteLine("Getting submission from database");
            // Let the user know we're processing their submission, if they check on it
            Submission submission = await getSubmissionAndMarkItProcessing(message.Submission);
            await downloadAndPrepareCompetitionAndSubmissionFiles(message.Competition, message.Submission);

            // Extract results and label for upload
            // https://www.newtonsoft.com/json/help/html/ReadJson.htm
            JObject resultData;
            string output = "";
            try
            {
                // Run the competition
                Console.WriteLine("Running the competition");
                string dockerPath = Path.Combine(homeDirectory, "run");
                SystemOperations.BuildDockerImage();
                ProcessStartInfo startInfo = SystemOperations.CreateSandboxDockerStartProcess();
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardError = true;
                //startInfo.WorkingDirectory = Path.Combine(homeDirectory, Workspace);
                Process process = Process.Start(startInfo);

                if (!process.WaitForExit(30000))
                {
                    Console.WriteLine("The process did not exit as expected, killing");
                    process.Kill();
                }
                output = process.StandardOutput.ReadToEnd();
                output += process.StandardError.ReadToEnd();

                Console.WriteLine("Parsing the JSON file left by the judging script");
                SystemOperations.CopyFileFromContainer("/workspace/result.json", "workspace/result.json");
                resultData = JObject.Parse(File.ReadAllText(Path.Combine(homeDirectory, Workspace, "result.json")));
                resultData["type"] = "finished";
                resultData["output"] = output;
            }
            catch (Exception e)
            {
                Console.WriteLine("Got the following exception: " + e.Message);
                resultData = new JObject
                    {
                        { "type", "error" },
                        { "what", e.Message },
                        { "output", output }
                    };
                SystemOperations.KillContainer();
            }

            await setSubmissionResultsAndUpdateIt(submission, resultData);
            SystemOperations.DisposeContainer();

            ShouldSleep = false;
        }

        private async Task<Submission> getSubmissionAndMarkItProcessing(Guid submissionId)
        {
            Submission submission = await database.GetSubmission(submissionId);
            Console.WriteLine("Updating submission status");
            submission.Status = SubmissionStatus.Processing;
            await database.UpdateSubmission(submission);
            return submission;
        }

        private async Task<Submission> setSubmissionResultsAndUpdateIt(Submission submission, JObject results)
        {
            Console.WriteLine("Updating the result information in the database");
            submission.Tests = results;
            if (results.HasValues && results.ContainsKey("score"))
            {
                submission.Score = results.Value<double?>("score") ?? 0;
            }
            submission.Status = SubmissionStatus.Completed;
            await database.UpdateSubmission(submission);
            return submission;
        }

        private async Task downloadAndPrepareCompetitionAndSubmissionFiles(Guid competitionId, Guid submissionId)
        {
            // Download the submission and competition and extract them
            string scriptFilePath = Path.Combine(homeDirectory, competitionId.ToString("D") + ".zip");
            string submissionFilePath = Path.Combine(homeDirectory, submissionId.ToString("D") + ".zip");

            await storage.DownloadScript(competitionId, scriptFilePath);
            await storage.DownloadSubmission(submissionId, submissionFilePath);

            ZipFile.ExtractToDirectory(scriptFilePath, Path.Combine(homeDirectory, Workspace, Competition));
            ZipFile.ExtractToDirectory(submissionFilePath, Path.Combine(homeDirectory, Workspace, Submission));
        }

        private void purgeWorkspace()
        {
            string workspaceDir = Path.Join(homeDirectory, Workspace);
            if (Directory.Exists(workspaceDir))
            {
                Directory.Delete(workspaceDir, true);
            }
            Directory.CreateDirectory(workspaceDir);
        }
    }
}
