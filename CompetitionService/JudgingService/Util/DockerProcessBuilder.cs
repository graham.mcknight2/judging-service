﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JudgingService.Util
{
    public class DockerProcessBuilder
    {
        private bool hasPidLimit;
        private int pidLimit;
        private List<string> capabilities;
        private bool hasMemoryLimit;
        private int memoryLimit;

        public string DockerImageName { get; private set; }

        public int PidLimit
        {
            get
            {
                return pidLimit;
            }
            set
            {
                hasPidLimit = true;
                pidLimit = value;
            }
        }

        public bool NetworkDisabled { get; set; }
        public bool CapabilitiesDropped { get; set; }

        public string[] WhitelistedCapabilities
        {
            get
            {
                return capabilities.ToArray();
            }
        }

        public int MemoryLimitInMegabytes
        {
            get
            {
                return memoryLimit;
            }
            set
            {
                hasMemoryLimit = true;
                memoryLimit = value;
            }
        }

        public DockerProcessBuilder(string imageName)
        {
            DockerImageName = imageName;
            capabilities = new List<string>();
        }

        public void WhitelistCapability(string capability)
        {
            capabilities.Add(capability);
        }

        public string GetDockerStartArguments()
        {
            StringBuilder arguments = new StringBuilder();
            arguments.Append("run --name " + DockerImageName);

            if (NetworkDisabled)
            {
                arguments.Append(" --network none");
            }

            if (hasPidLimit)
            {
                arguments.Append(" --pids-limit ").Append(PidLimit);
            }

            if (hasMemoryLimit)
            {
                arguments.Append(" --memory=").Append(memoryLimit).Append("m");
            }

            if (CapabilitiesDropped)
            {
                arguments.Append(" --cap-drop=all");
            }

            foreach (string capability in capabilities)
            {
                arguments.Append(" --cap-add=").Append(capability);
            }

            arguments.Append(" ").Append(DockerImageName);

            return arguments.ToString();
        }

        public override string ToString()
        {
            return "docker " + GetDockerStartArguments();
        }
    }
}
