﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace JudgingService.Util
{
    /// <summary>
    /// Class to abstract away platform dependent operations.
    /// </summary>
    class SystemOperations
    {
        public static string GetHomeDirectory()
        {
            // https://stackoverflow.com/questions/1143706/getting-the-path-of-the-home-directory-in-c
            if (IsUnixPlatform())
            {
                return Environment.GetEnvironmentVariable("HOME");
            }
            else
            {
                return Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
            }
        }

        public static ProcessStartInfo CreateProcessStartInfo(string path)
        {
            string shellName = IsUnixPlatform() ? "/bin/bash/sh" : "powershell";
            if (!path.EndsWith(GetScriptFileExtension()))
            {
                path += GetScriptFileExtension();
            }
            return new ProcessStartInfo(shellName, path);
        }

        public static void BuildDockerImage()
        {
            // 4 minutes should be enough time to build an alpine container,
            // but you can never really know.
            runProcessTimeout("docker", "build -f Submission.Dockerfile . -t submission-container", 24000);
        }

        public static ProcessStartInfo CreateSandboxDockerStartProcess()
        {
            // Remove networking capability
            DockerProcessBuilder builder = new DockerProcessBuilder("submission-container");
            builder.NetworkDisabled = true;
            builder.PidLimit = 20;
            builder.MemoryLimitInMegabytes = 384;
            builder.CapabilitiesDropped = true;
            builder.WhitelistCapability("kill");
            Console.WriteLine("Running: " + builder.ToString());
            return new ProcessStartInfo("docker", builder.GetDockerStartArguments());
        }
        
        public static void CopyFileFromContainer(string source, string dest)
        {
            runProcessTimeout("docker", " cp submission-container:" + source + " " + dest, 30000);
        }

        public static void KillContainer()
        {
            runProcessTimeout("docker", "kill submission-container", 30000);
        }

        public static void DisposeContainer()
        {
            runProcessTimeout("docker", " rm submission-container", 30000);
            runProcessTimeout("docker", " rmi --force submission-container", 30000);
        }

        private static void runProcessTimeout(string name, string args, int time)
        {
            Process process = Process.Start(name, args);
            process.WaitForExit(time);
            if (!process.HasExited)
            {
                process.Kill();
            }
        }

        public static string GetScriptFileExtension()
        {
            if (IsUnixPlatform())
            {
                return ".sh";
            }
            else
            {
                return ".ps1";
            }
        }

        public static bool IsUnixPlatform()
        {
            return Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX;
        } 
    }
}
