FROM openjdk:8-jdk-alpine
COPY workspace/ /workspace
RUN chmod -R a+rwx workspace
RUN adduser -D userpriviledge
USER userpriviledge

WORKDIR /workspace
CMD ["/bin/sh", "./contest/run.sh"]