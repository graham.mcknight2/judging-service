﻿using AzureServices;
using Commons.Models;
using JudgingService.Util;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading;
using System.Threading.Tasks;

namespace JudgingService
{
    class Program
    {
        public static void Main(string[] args)
        {
            Task.Run(async () => {
                    try { await Start(); }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            ).Wait();
        }

        public static async Task Start() {
            // Load the config file
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("appsettings.json");
            IConfiguration config = builder.Build();

            string homeDirectory = System.Environment.CurrentDirectory;

            Console.WriteLine(homeDirectory);

            AzureStorageClient storage = new AzureStorageClient(config.GetConnectionString("AzureStorage"));
            ICompetitionDatabase database = new CosmosDBCompetitionDatabase(config);

            await ((AzureStorageClient)storage).Initialize();
            await ((CosmosDBCompetitionDatabase)database).Initialize();

            SubmissionHandler service = new SubmissionHandler(database, storage, homeDirectory);

            while (true)
            {
                await service.HandleNextMessage();
                if (service.ShouldSleep)
                {
                    // TODO: Get from config file
                    Thread.Sleep(5000);
                }
            }
        }
    }
}
