﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureServices;
using CompetitionService.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CompetitionService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .AddJsonOptions(options => {
                    options.SerializerSettings.Error = (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args) =>
                    {
                        Console.WriteLine(sender);
                        Console.WriteLine(args.ErrorContext.Error.Message);
                    };
                }); ;

            //services.AddDbContext<CompetitionDatabase>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("CompetitionDatabase")));

            // Allows the dependency injection to inject the ICompetitionDatabase instead of
            // requiring a CompetitionDatabase, allowing for more testing options later.
            services.AddScoped<ICompetitionDatabase, CosmosDBCompetitionDatabase>();

            services.AddScoped<IAzureStorageClient, AzureStorageClient>(provider =>
                new AzureStorageClient(Configuration.GetConnectionString("AzureStorage")));

            services.AddScoped<UserAuthenticationInfo>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
            ICompetitionDatabase database, IAzureStorageClient storage)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            }
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMiddleware<UserAuthenticationMiddleware>();
            Task.Run(() => ((AzureStorageClient)storage).Initialize()).Wait();
            Task.Run(() => ((CosmosDBCompetitionDatabase)database).Initialize()).Wait();

            app.UseMvc();
        }
    }
}
