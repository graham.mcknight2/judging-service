﻿using Commons.Models.Auth;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace CompetitionService.Middleware
{
    public class UserAuthenticationMiddleware
    {
        private RequestDelegate next;

        public UserAuthenticationMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context, UserAuthenticationInfo authInfo)
        {
            Console.WriteLine(context.Request.Path);
            if (context.Request.Path.StartsWithSegments("/api/Login"))
            {
                await next.Invoke(context);
                return;
            }

            if (context.Request.Headers.ContainsKey("Token"))
            {
                string token = context.Request.Headers["Token"];
                if (token == null || !Token.IsWellFormedToken(token))
                {
                    context.Response.StatusCode = 401;
                    return;
                }

                Token parsedToken = Token.Parse(token);

                if (parsedToken.Header.Expires < DateTime.Now)
                {
                    context.Response.StatusCode = 401;
                    return;
                }

                authInfo.UserId = parsedToken.Body.UserId;
                await next.Invoke(context);
            }
            else
            {
                context.Response.StatusCode = 401;
            }
        }
    }
}
