﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompetitionService.Middleware {
    public class UserAuthenticationInfo
    {
        public Guid UserId { get; set; }
    }
}
