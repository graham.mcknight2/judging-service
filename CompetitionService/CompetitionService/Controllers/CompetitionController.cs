﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureServices;
using Commons.Models;
using CompetitionService.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompetitionService.Controllers
{
    [Produces("application/json")]
    [Route("api/Competition")]
    public class CompetitionController : Controller
    {
        private ICompetitionDatabase database;
        private IAzureStorageClient storage;

        public CompetitionController(ICompetitionDatabase database, IAzureStorageClient storage)
        {
            this.database = database;
            this.storage = storage;
        }

        /// <summary>
        /// Returns all upcoming competitions.
        /// </summary>
        /// <status code="200">Returns a list of upcoming competitions.</status>
        [HttpGet("upcoming")]
        public async Task<IActionResult> GetUpcomingCompetitions()
        {
            return new ObjectResult(await database.CompetitionsAfterDate(DateTime.Now));
        }

        /// <summary>
        /// Creates a new competition in the database.
        /// </summary>
        /// <param name="competition">The competition object in the request body.</param>
        /// <param name="authInfo">Authorization information from header.</param>
        /// <response code="400">Competition is ill-formed.</response>
        /// <response code="201">Competition successfully created.</response>
        [HttpPost]
        public async Task<IActionResult> NewCompetition([FromBody] Competition competition,
            [FromServices] UserAuthenticationInfo authInfo)
        {

            // TODO: Forbidden if their account doesn't have
            // competition permissions.

            User user = await database.GetUser(authInfo.UserId);
            if (user == null || !user.Contributor)
            {
                return Forbid();
            }

            if (competition == null)
            {
                return BadRequest();
            }

            competition.Id = Guid.NewGuid();

            await database.AddCompetition(competition);

            return Created("api/contest/" + competition.Id.ToString("D"), competition);
        }

        /// <summary>
        /// Returns information on a given competition by id.
        /// </summary>
        /// <param name="id">The guid for this particular competition, as a url parameter.</param>
        /// <response code="400">Competition id is ill formed or missing.</response>
        /// <response code="404">Competition does not exist.</response>
        /// <response code="200">Competition returned in response body.</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCompetition(Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest();
            }

            Competition competition = await database.GetCompetition(id);

            if (competition == null)
            {
                return NotFound();
            }

            return new ObjectResult(competition);
        }

        [HttpGet("{id}/script")]
        public async Task<IActionResult> GetScriptUploadSas ([FromServices] UserAuthenticationInfo authInfo, Guid id)
        {
            // TODO: User ownership checking, authentication as above.
            User user = await database.GetUser(authInfo.UserId);
            if (user == null || !user.Contributor)
            {
                return Forbid();
            }

            if (id == Guid.Empty)
            {
                return BadRequest();
            }

            Competition competition = await database.GetCompetition(id);
            if (competition == null)
            {
                return NotFound();
            }

            string sasUri = await storage.AuthorizeContestUpload(id);
            return new ObjectResult(sasUri);
        }
    }
}