﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureServices;
using Commons.Models;
using CompetitionService.Middleware;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CompetitionService.Controllers
{
    [Produces("application/json")]
    [Route("api/Submission")]
    public class SubmissionController : Controller
    {
        private ICompetitionDatabase database;
        private IAzureStorageClient storage;

        public SubmissionController(ICompetitionDatabase database, IAzureStorageClient storage)
        {
            this.database = database;
            this.storage = storage;
        }

        /// <summary>
        /// Creates a new submission for a given competition.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <param name="submission">Submission information in body. Contains only competitionId.</param>
        /// <response code="400">Submission ill formed or nonexistant.</response>
        /// <response code="404">Competition submission is being submitted to does not exist.</response>
        /// <response code="201">Competition correctly added to database.</response>
        [HttpPost]
        public async Task<IActionResult> NewSubmission(
            [FromServices] UserAuthenticationInfo authInfo, [FromBody] Submission submission)
        {
            if (submission == null)
            {
                return BadRequest();
            }

            // TODO: User authentication/permissions.

            submission.UserId = authInfo.UserId;

            Competition competition = await database.GetCompetition(submission.CompetitionId);
            if (competition == null)
            {
                return NotFound();
            }

            submission.Id = Guid.NewGuid();
            submission.Status = SubmissionStatus.Opened;
            await database.AddSubmission(submission);

            return Created("api/submission/" + submission.Id.ToString("D"), submission);
        }

        /// <summary>
        /// Gets a list of all submissions that have been made by the logged in user.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <response "200">Submissions for user returned in body.</response>
        [HttpGet("user")]
        public async Task<IActionResult> GetListOfSubmissionsForCurrentUser([FromServices] UserAuthenticationInfo authInfo)
        {
            return new ObjectResult(await database.SubmissionsForUser(authInfo.UserId));
        }

        /// <summary>
        /// Gets a list of all submissions that have been made by the given user.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <response "200">Submissions for user returned in body.</response>
        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetListOfSubmissionsUser([FromServices] UserAuthenticationInfo authInfo, Guid id)
        {
            if (authInfo.UserId != id)
            {
                return Forbid();
            }
            // TODO: Better checks for whether the user has access to the submissions found.
            return new ObjectResult(await database.SubmissionsForUser(id));
        }

        /// <summary>
        /// Returns details such as the current status of a submission.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <param name="id">Id of the submission to get information about, in url parameter.</param>
        /// <response code="400">Id is ill formed or missing.</response>
        /// <response code="404">Submission requested does not exist.</response>
        /// <response code="403">A different user than the one who owns the submission is requesting information.</response>
        /// <response code="200">Submission returned correctly in body.</response>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubmission([FromServices] UserAuthenticationInfo authInfo, Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest();
            }

            Submission submission = await database.GetSubmission(id);

            if (submission == null)
            {
                return NotFound();
            }

            // TODO: Some changes may be necessary for this scheme.
            if (submission.UserId != authInfo.UserId)
            {
                return Forbid();
            }

            return new ObjectResult(submission);
        }

        /// <summary>
        /// Provides an upload SAS url to which a user can upload their submission code.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <param name="id">Id of submission to upload code for.</param>
        /// <response code="400">Id is ill formed.</response>
        /// <response code="404">Submission requested does not exist.</response>
        /// <response code="403">User attempting to upload SAS does not own submission.</response>
        /// <response code="200">SAS url provided in body.</response>
        [HttpGet("{id}/token")]
        public async Task<IActionResult> GetSubmissionUploadSas([FromServices] UserAuthenticationInfo authInfo, Guid id)
        {
            // TODO: User ownership and authentication.

            if (id == Guid.Empty)
            {
                return BadRequest();
            }

            Submission submission = await database.GetSubmission(id);
            if (submission == null)
            {
                return NotFound();
            }

            if (submission.UserId != authInfo.UserId)
            {
                return Forbid();
            }

            string sasUri = await storage.AuthorizeSubmissionUpload(id);
            return new ObjectResult(sasUri);
        }

        /// <summary>
        /// Begins the process of evaluating a submission with the competition's
        /// given script.
        /// </summary>
        /// <param name="authInfo">Authorization information, parsed from header.</param>
        /// <param name="id">Id of submission to submit.</param>
        /// <response code="400">Id is ill formed.</response>
        /// <response code="404">Submission submitted does not exist.</response>
        /// <response code="403">User attempting to submit submission belonging to another user.</response>
        /// <response code="422">Submission has already been submitted.</response>
        /// <response code="202">The submission has been queued.</response>
        [HttpPut("{id}")]
        public async Task<IActionResult> Submit([FromServices] UserAuthenticationInfo authInfo, Guid id)
        {
            if (id == Guid.Empty)
            {
                return BadRequest();
            }

            Submission submission = await database.GetSubmission(id);
            if (submission == null)
            {
                return NotFound();
            }

            // TODO: Check user ownership of the submission.
            if (submission.UserId != authInfo.UserId)
            {
                Console.WriteLine("Wrong id, got " + authInfo.UserId + " expecting " + submission.UserId);
                return Forbid();
            }

            // A user can only try to submit their work once.
            if (submission.Status != SubmissionStatus.Opened)
            {
                // Unprocessable Entity response.
                return StatusCode(422);
            }

            await storage.AddSubmissionToQueue(submission.Id, submission.CompetitionId, authInfo.UserId);
            submission.Status = SubmissionStatus.Queued;
            submission.Submitted = DateTime.Now;
            await database.UpdateSubmission(submission);

            return Accepted();
        }
    }
}