﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureServices;
using Commons.Auth;
using Commons.Models;
using Commons.Models.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CompetitionService.Controllers
{
    [Produces("application/json")]
    [Route("api/Login")]
    public class LoginController : Controller
    {
        private ICompetitionDatabase database;

        public LoginController(ICompetitionDatabase database)
        {
            this.database = database;
        }

        /// <summary>
        /// Verifies a user's login information and provides a token
        /// if the username and password are correct.
        /// </summary>
        /// <param name="login">A username password pair, in body.</param>
        /// <response code="400">Login pair is ill formed.</response>
        /// <response code="404">Email username doesn't exist.</response>
        /// <response code="403">Password is incorrect.</response>
        /// <response code="200">Login successful, token in body.</response>
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginPair login)
        {
            if (login == null)
            {
                return BadRequest();
            }

            User user = await database.GetUser(login.Email);

            // If the database can't find a user with that email, then
            if (user == null)
            {
                return NotFound();
            }

            var securityManager = BasePasswordSecurityManager.GetPasswordSecurity(user.PasswordVersion);

            // If the user entered an incorrect password, we should let the client know
            if (!securityManager.IsCorrectPassword(login.Password, user.PasswordHash))
            {
                return Unauthorized();
            }

            // Required for testing
            // TODO: Find a better fix for this
            string ipAddress;
            if (Request != null)
            {
                ipAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            }
            else
            {
                ipAddress = "DebugIpAddress";
            }

            Token token = new Token
            {
                Header = new TokenHeader
                {
                    Issued = DateTime.Now,
                    Expires = DateTime.Now.AddMinutes(30),
                    Issuer = "TODO",
                    ClientIpAdress = ipAddress
                },

                Body = new TokenBody
                {
                    UserId = user.Id
                }
            };

            return Content(token.ToString());
        }

        /// <summary>
        /// Creates a new user account with a given username and password.
        /// </summary>
        /// <param name="login">Username/email and password pair. In body.</param>
        /// <response code="400">Login ill formed or missing.</response>
        /// <response code="409">Username/email already taken.</response>
        /// <response code="201">Account created successfully.</response>
        [HttpPost("new")]
        public async Task<IActionResult> NewUser([FromBody] LoginPair login)
        {

            // If the user object is ill-formed (doesn't have the necessary fields
            // or has something extra, it will resolve to null.
            if (login == null)
            {
                // Return a 400 response.
                return BadRequest();
            }

            if (await database.ExistsUser(login.Email))
            {
                Console.WriteLine("Refusing because the email already exists");
                return new StatusCodeResult(StatusCodes.Status409Conflict);
            }

            User user = new User
            {
                // Give the user a unique id
                Id = Guid.NewGuid(),
                Email = login.Email,

                Contributor = false, // For now, contributors can only be set from the DB

                // Security features for the password
                PasswordHash = BasePasswordSecurityManager.GetCurrentPasswordSecurity().EncryptPassword(login.Password),
                PasswordVersion = BasePasswordSecurityManager.CurrentPasswordProtocolVersion
            };

            await database.AddUser(user);

            // Return a 201 with the uri (relative to the api root) to where the client
            // can make a GET to pull user information.
            // TODO: Currently this created does not point to a new resource.
            return Created("api/User/" + user.Id.ToString("D"),
                new User
                {
                    Email = user.Email,
                    Id = user.Id
                });
        }

        /// <summary>
        /// Parses a token for the user and gives them their user id.
        /// </summary>
        /// <param name="token">The authentication given to the client
        /// after login.</param>
        /// <returns>The guid id of the user.</returns>
        [HttpGet("{token}")]
        public IActionResult GetUserIdFromToken(string token)
        {
            if (token == null || !Token.IsWellFormedToken(token))
            {
                return BadRequest();
            }

            Token parsedToken = Token.Parse(token);
            return new ObjectResult(token);
        }
    }
}
