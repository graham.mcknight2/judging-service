﻿using Commons.Models;
using CompetitionService.Controllers;
using CompetitionService.Middleware;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Mocking;
using Xunit;

namespace Tests
{
    public class SubmissionControllerTester : BaseCompetitionServiceFixture
    {
        [Fact]
        public async Task CanCreateNewSubmission()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.NewSubmission(MakeUserAuth(), GetBasicSubmission());

            var createdResult = Assert.IsType<CreatedResult>(result);
            var submission = Assert.IsType<Submission>(createdResult.Value);

            Assert.NotNull(await database.GetSubmission(submission.Id));
        }

        [Fact]
        public async Task CreatingIllFormedSubmissionReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.NewSubmission(MakeUserAuth(), null);

            Assert.IsType<BadRequestResult>(result);
            Assert.False(database.ChangesSaved);
        }

        [Fact]
        public async Task UsingNonexistentCompetitionReturnsNotFound()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.NewSubmission(MakeUserAuth(),
                new Submission
                {
                    CompetitionId = GetUnusedGuid()
                });

            Assert.IsType<NotFoundResult>(result);
            Assert.False(database.ChangesSaved);

        }

        [Fact]
        public async Task CanGetSubmissionsForUser()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetListOfSubmissionsForCurrentUser(MakeUserAuth());

            var objectResult = Assert.IsType<ObjectResult>(result);
            var submissions = Assert.IsAssignableFrom<ICollection<Submission>>(objectResult.Value);
            Assert.Equal(1, submissions.Count);
        }

        [Fact]
        public async Task DoesntGetSubmissionsForOtherUsers()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetListOfSubmissionsForCurrentUser(
                new UserAuthenticationInfo
                {
                    UserId = GetUnusedGuid()
                });

            var objectResult = Assert.IsType<ObjectResult>(result);
            var submissions = Assert.IsAssignableFrom<ICollection<Submission>>(objectResult.Value);
            Assert.Equal(0, submissions.Count);
        }

        [Fact]
        public async Task CanGetSubmissionById()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmission(MakeUserAuth(), GetSubmissionGuid());

            var objectResult = Assert.IsType<ObjectResult>(result);
            var submission = Assert.IsType<Submission>(objectResult.Value);
        }

        [Fact]
        public async Task GettingSubmissionWithIllFormedIdReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmission(MakeUserAuth(), Guid.Empty);

            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task GettingNonexistentSubmissionReturnsNotFound()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmission(MakeUserAuth(), GetUnusedGuid());

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GettingAnotherUsersSubmissionReturnsForbidden()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmission(
                new UserAuthenticationInfo
                {
                    UserId = GetUnusedGuid()
                }, GetSubmissionGuid());

            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async Task CanGetSubmissionUploadSas()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmissionUploadSas(MakeUserAuth(), GetSubmissionGuid());

            var objectResult = Assert.IsType<ObjectResult>(result);
            string sasToken = Assert.IsType<string>(objectResult.Value);

            Assert.Equal("submission:" + GetSubmissionGuid(), sasToken);
        }

        [Fact]
        public async Task GettingUploadSasWithIllFormedIdReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmissionUploadSas(MakeUserAuth(), Guid.Empty);

            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task GettingUploadSasOnNonexistentSubmissionReturnsNotFound()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmissionUploadSas(MakeUserAuth(), GetUnusedGuid());

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GettingUploadSasForOtherUsersSubmissionReturnsForbidden()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.GetSubmissionUploadSas(
                new UserAuthenticationInfo
                {
                    UserId = GetUnusedGuid()
                }, GetSubmissionGuid());

            Assert.IsType<ForbidResult>(result);
        }

        [Fact]
        public async Task CanSubmitSubmissionToBeProcessed()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.Submit(MakeUserAuth(), GetSubmissionGuid());

            Assert.IsType<AcceptedResult>(result);

            Assert.Equal(SubmissionStatus.Queued, (await database.GetSubmission(GetSubmissionGuid())).Status);
            Assert.Equal(SubmissionStatus.Queued, (await database.GetSubmission(GetSubmissionGuid())).Status);

            Assert.NotNull(storage.GetQueueElement(0));
            Assert.Equal(GetCompetitionGuid(), storage[0, MockedAzureStorageClient.Competition]);
            Assert.Equal(GetSubmissionGuid(), storage[0, MockedAzureStorageClient.Submission]);
            Assert.Equal(GetUserGuid(), storage[0, MockedAzureStorageClient.User]);
        }

        [Fact]
        public async Task SubmittingWithIllFormedIdReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.Submit(MakeUserAuth(), Guid.Empty);

            Assert.IsType<BadRequestResult>(result);
            Assert.False(database.ChangesSaved);
        }

        [Fact]
        public async Task SubmittingNonexistentSubmissionReturnsNotFound()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.Submit(MakeUserAuth(), GetUnusedGuid());

            Assert.IsType<NotFoundResult>(result);
            Assert.False(database.ChangesSaved);
        }

        [Fact]
        public async Task SubmittingSomeoneElsesSubmissionReturnsForbidden()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.Submit(
                new UserAuthenticationInfo
                {
                    UserId = GetUnusedGuid()
                }, GetSubmissionGuid());

            Assert.IsType<ForbidResult>(result);
            Assert.False(database.ChangesSaved);
        }

        [Fact]
        public async Task ResubmittingSubmissionReturnsUnprocessableEntity()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            (await database.GetSubmission(GetSubmissionGuid())).Status = SubmissionStatus.Queued;

            SubmissionController controller = new SubmissionController(database, storage);

            IActionResult result = await controller.Submit(MakeUserAuth(), GetSubmissionGuid());

            var statusCodeResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(422, statusCodeResult.StatusCode);

            Assert.False(database.ChangesSaved);
        }
    }
}
