﻿using Commons.Models;
using CompetitionService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Mocking;
using Xunit;

namespace Tests
{
    public class CompetitionControllerTester : BaseCompetitionServiceFixture
    {
        [Fact]
        public async Task CanCreateNewCompetition()
        {
            MockedCompetitionDatabase database = await MakeDatabaseWithContributor();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.NewCompetition(GetBasicCompetition(), MakeUserAuth());

            var createdResult = Assert.IsType<CreatedResult>(result);
            var competition = Assert.IsType<Competition>(createdResult.Value);

            Assert.NotNull(await database.GetCompetition(competition.Id));
        }

        [Fact]
        public async Task NonContributorsCannotCreateCompetitions()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);
            var competition = new Competition
            {
                Id = GetUnusedGuid(),
                Deadline = DateTime.Now.AddDays(2),
                Description = "Description",
                Title = "Name",
                EmbedUrl = "Url"
            };
            IActionResult result = await competitionController.NewCompetition(GetBasicCompetition(), MakeNonContributorUserAuth());

            Assert.IsType<ForbidResult>(result);
            Assert.Null(await database.GetCompetition(GetUnusedGuid()));
        }

        [Fact]
        public async Task IllFormedNewCompetitionReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakeDatabaseWithContributor();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.NewCompetition(null, MakeUserAuth());

            Assert.IsType<BadRequestResult>(result);
            Assert.False(database.ChangesSaved);
        }

        [Fact]
        public async Task CanGetExistingCompetition()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetCompetition(GetCompetitionGuid());

            var objectResult = Assert.IsType<ObjectResult>(result);
            var competition = Assert.IsType<Competition>(objectResult.Value);

            Assert.Equal<Guid>(GetCompetitionGuid(), competition.Id);
        }

        [Fact]
        public async Task GettingNonexistantCompetitionReturnsNotFound()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetCompetition(GetUnusedGuid());

            var notFoundResult = Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task GettingIllFormedCompetitionGuidReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetCompetition(Guid.Empty);

            var notFoundResult = Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task CanGetScriptUploadSasForCompetition()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetScriptUploadSas(MakeUserAuth(), GetCompetitionGuid());

            var objectResult = Assert.IsType<ObjectResult>(result);
            string sas = Assert.IsType<string>(objectResult.Value);

            Assert.Equal("contest:" + GetCompetitionGuid(), sas);
        }

        [Fact]
        public async Task GettingUploadSasForIllFormedCompetitionReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetScriptUploadSas(MakeUserAuth(), Guid.Empty);

            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task GettingUploadSasForNonexistantCompetitionReturnsBadRequest()
        {
            MockedCompetitionDatabase database = await MakePopulatedCompetitionDatabase();
            MockedAzureStorageClient storage = MakeAzureStorage();

            CompetitionController competitionController = new CompetitionController(database, storage);

            IActionResult result = await competitionController.GetScriptUploadSas(MakeUserAuth(), GetUnusedGuid());

            Assert.IsType<NotFoundResult>(result);
        }
    }
}
