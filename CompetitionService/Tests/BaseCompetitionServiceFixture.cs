﻿using Commons.Models;
using CompetitionService.Middleware;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Mocking;

namespace Tests
{
    public abstract class BaseCompetitionServiceFixture
    {
        protected Guid GetCompetitionGuid()
        {
            return new Guid("606b81fd-aee9-4347-bbbf-120233ada600");
        }

        protected Guid GetSubmissionGuid()
        {
            return new Guid("a30f3c16-1078-468e-9a21-bcf9e803baef");
        }

        protected Guid GetUserGuid()
        {
            return new Guid("de3f1a3b-5270-4618-9d85-5be362590112");
        }

        protected Guid GetNonContributorUserGuid()
        {
            return new Guid("8d79c3ae-bc5b-44dc-b0b0-39dec999cd5e");
        }

        protected Guid GetUnusedGuid()
        {
            return new Guid("1a902e44-6b6e-4ca6-b72b-74e559731af8");
        }

        protected Competition GetBasicCompetition()
        {
            return new Competition
            {
                Deadline = DateTime.Now.AddDays(2),
                Description = "Description",
                Title = "Name",
                EmbedUrl = "Url"
            };
        }

        protected Submission GetBasicSubmission()
        {
            return new Submission
            {
                CompetitionId = GetCompetitionGuid()
            };
        }

        protected MockedCompetitionDatabase MakeBlankCompetitionDatabase()
        {
            return new MockedCompetitionDatabase();
        }

        protected async Task<MockedCompetitionDatabase> MakeDatabaseWithContributor()
        {
            var database = new MockedCompetitionDatabase();

            await database.AddUser(new User
            {
                Id = GetUserGuid(),
                Email = "email.@email.com",
                Contributor = true
            });

            return database;
        }

        protected async Task<MockedCompetitionDatabase> MakePopulatedCompetitionDatabase()
        {
            var database = new MockedCompetitionDatabase();

            await database.AddUser(new User
            {
                Id = GetUserGuid(),
                Email = "email.@email.com",
                Contributor = true
            });

            await database.AddUser(new User
            {
                Id = GetNonContributorUserGuid(),
                Email = "user.@email.com",
                Contributor = false
            });

            await database.AddCompetition(new Competition
            {
                Id = GetCompetitionGuid(),
                Deadline = DateTime.Now.AddDays(2),
                Description = "Description",
                Title = "Name",
                EmbedUrl = "Url"
            });

            await database.AddSubmission(new Submission
            {
                Id = GetSubmissionGuid(),
                CompetitionId = GetCompetitionGuid(),
                Status = SubmissionStatus.Opened,
                Submitted = DateTime.Now.AddHours(-1),
                UserId = GetUserGuid()
            });

            return database;
        }

        protected MockedAzureStorageClient MakeAzureStorage()
        {
            return new MockedAzureStorageClient();
        }

        protected UserAuthenticationInfo MakeUserAuth()
        {
            return new UserAuthenticationInfo
            {
                UserId = GetUserGuid()
            };
        }

        protected UserAuthenticationInfo MakeNonContributorUserAuth()
        {
            return new UserAuthenticationInfo
            {
                UserId = GetNonContributorUserGuid()
            };
        }
    }
}
