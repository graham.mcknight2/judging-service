using Commons;
using Commons.Auth;
using System;
using Xunit;

namespace Tests
{
    public class PasswordSecurityTester
    {
        [Fact]
        public void EncryptedMessagesCanBeDecrypted()
        {
            string message = "test123test;";

            var encrypter = new PasswordEncrypter(Secrets.PasswordKey, Secrets.PasswordIV);
            string encrypted = encrypter.Encrypt(message);

            Assert.Equal(message, encrypter.Decrypt(encrypted));
        }

        [Fact]
        public void EncryptionDoesntDoNothing()
        {
            string message = "test123test;";

            var encrypter = new PasswordEncrypter(Secrets.PasswordKey, Secrets.PasswordIV);
            string encrypted = encrypter.Encrypt(message);

            Assert.NotEqual(message, encrypted);
        }

        [Fact]
        public void HashingDoesntDoNothing()
        {
            string password = "test123test";
            string salt = "saltsalt";

            var hasher = new PasswordHasher();
            string hashedPassword = hasher.Hash(password, salt);

            Assert.NotEqual(password, hashedPassword);
            Assert.NotEqual(salt + ":" + password, hashedPassword);
        }

        [Fact]
        public void HashingWithANullSaltProducesOne()
        {
            string password = "test123test";

            var hasher = new PasswordHasher();
            string hashedPassword = hasher.Hash(password, null);

            string salt = hasher.GetSalt(hashedPassword);

            Assert.NotEmpty(salt);
            Assert.Contains(salt + ":", hashedPassword);
        }

        [Fact]
        public void HashingCorrectlyVerifiesPasswordUsingSalt()
        {
            string password = "test123test";

            var hasher = new PasswordHasher();
            string hashedPassword = hasher.Hash(password, null);
            string salt = hasher.GetSalt(hashedPassword);

            Assert.Equal(hashedPassword, hasher.Hash(password, salt));
            Assert.NotEqual(hashedPassword, hasher.Hash(password, null));
        }
    }
}
