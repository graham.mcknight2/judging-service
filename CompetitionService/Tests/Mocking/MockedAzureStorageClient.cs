﻿using AzureServices;
using Commons.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Mocking
{
    public class MockedAzureStorageClient : IAzureStorageClient
    {
        public const int Submission = 0;
        public const int Competition = 1;
        public const int User = 2;

        private List<Tuple<Guid, Guid, Guid>> queue;
        private List<SubmissionMessage> messages;

        public MockedAzureStorageClient()
        {
            queue = new List<Tuple<Guid, Guid, Guid>>();
            messages = new List<SubmissionMessage>();
        }

        public Guid this[int index, int component]
        {
            get
            {
                switch (component)
                {
                    case Submission:
                        return GetQueueElement(index).Item1;
                    case Competition:
                        return GetQueueElement(index).Item2;
                    case User:
                        return GetQueueElement(index).Item3;
                    default:
                        throw new ArgumentException("Incorrect component value " + component);
                }
            }
        }

        public Tuple<Guid, Guid, Guid> GetQueueElement(int index)
        {
            if (queue.Count > index)
            {
                return queue[index];
            }
            else
            {
                return null;
            }
        }

        public Task AddSubmissionToQueue(Guid submissionId, Guid contestId, Guid user)
        {
            return Task.Run(() =>
            {
                queue.Add(new Tuple<Guid, Guid, Guid>(submissionId, contestId, user));
                messages.Add(new SubmissionMessage
                {
                    Submission = submissionId,
                    Competition = contestId,
                    User = user
                });
            });
        }

        public Task<string> AuthorizeContestUpload(Guid id)
        {
            return Task.Run(() =>
            {
                return "contest:" + id;
            });
        }

        public Task<string> AuthorizeSubmissionUpload(Guid id)
        {
            return Task.Run(() =>
            {
                return "submission:" + id;
            });
        }

        public Task<SubmissionMessage> NextMessage()
        {
            return Task.Run(() =>
            {
                if (messages.Count == 0)
                {
                    return null;
                }

                SubmissionMessage topMessage = messages[0];
                messages.RemoveAt(0);
                return topMessage;
            });
        }

        public Task DownloadScript(Guid id, string location)
        {
            // Do nothing; the resources are already in the correct location.
            return Task.Run(() => GetHashCode());
        }

        public Task DownloadSubmission(Guid id, string location)
        {
            // Do nothing; the resources are already in the correct location.
            return Task.Run(() => GetHashCode());
        }
    }
}
