﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Tests.Mocking
{
    class SampleZipper
    {
        public static void ZipSamples()
        {
            string[] samples = Directory.GetDirectories("../../../Samples");
            foreach (string sample in samples)
            {
                string zipName = Path.GetFileName(sample) + ".zip";
                if (File.Exists(zipName))
                {
                    File.Delete(zipName);
                }

                ZipFile.CreateFromDirectory(sample, zipName);
            }
        }
    }
}
