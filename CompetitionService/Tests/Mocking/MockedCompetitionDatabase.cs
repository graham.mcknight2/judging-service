﻿using AzureServices;
using Commons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Mocking
{
    public class MockedCompetitionDatabase : ICompetitionDatabase
    {
        public bool ChangesSaved { get; private set; }

        private Dictionary<Guid, Competition> competitions;
        private Dictionary<Guid, Submission> submissions;
        private Dictionary<Guid, User> users;

        public MockedCompetitionDatabase()
        {
            ChangesSaved = false;
            competitions = new Dictionary<Guid, Competition>();
            submissions = new Dictionary<Guid, Submission>();
            users = new Dictionary<Guid, User>();
        }

        public void ResetChangesTracking()
        {
            ChangesSaved = false;
        }

        public Task AddCompetition(Competition competition)
        {
            return Task.Run(() =>
            {
                competitions.Add(competition.Id, competition);
            });
        }

        public Task AddSubmission(Submission submission)
        {
            return Task.Run(() =>
            {
                submissions.Add(submission.Id, submission);
            });
        }

        public Task<ICollection<Competition>> CompetitionsAfterDate(DateTime date)
        {
            return Task.Run<ICollection<Competition>>(() =>
            {
                return (from b in competitions where b.Value.Deadline > date select b.Value).ToList();
            });
        }

        public Task<Competition> GetCompetition(Guid id)
        {
            return Task.Run<Competition>(() =>
            {
                return competitions.GetValueOrDefault(id);
            });
        }

        public Task<Submission> GetSubmission(Guid id)
        {
            return Task.Run<Submission>(() =>
            {
                return submissions.GetValueOrDefault(id);
            });
        }

        public Task<ICollection<Submission>> SubmissionsForUser(Guid id)
        {
            return Task.Run<ICollection<Submission>>(() =>
            {
                return (from b in submissions where (b.Value.UserId == id) select b.Value).ToList();
            });
        }

        public int SaveChanges()
        {
            ChangesSaved = true;
            return 0;
        }

        public Task UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateSubmission(Submission submission)
        {
            return Task.Run(() =>
            {
                submissions[submission.Id] = submission;
            });
        }

        public Task AddUser(User user)
        {
            return Task.Run(() =>
            {
                users.Add(user.Id, user);
            });

        }

        public Task<bool> ExistsUser(string email)
        {
            return Task.Run<bool>(() =>
            {
                return users.Any(userPair => userPair.Value.Email == email);
            });
        }

        public Task<User> GetUser(Guid id)
        {
            return Task.Run<User>(() =>
            {
                User user = null;
                users.TryGetValue(id, out user);
                return user;
            });

        }

        public Task<User> GetUser(string email)
        {
            return Task.Run<User>(() =>
            {
                return users.First(userPair => userPair.Value.Email == email).Value;
            });
        }

        public Task UpdateCompetition(Competition competition)
        {
            throw new NotImplementedException();
        }
    }
}
