﻿using Commons.Models;
using JudgingService;
using JudgingService.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Mocking;
using Xunit;
using Xunit.Abstractions;

namespace Tests
{
    public class JudgingServiceTester : BaseJudgingServiceFixture
    {
        private readonly ITestOutputHelper output;
        public JudgingServiceTester(ITestOutputHelper output)
        {
            this.output = output;
        }

        [Fact]
        public async Task TestEmptyQueueCausesSleepAsync()
        {
            MockedCompetitionDatabase database = await MakeCompetitionDatabaseAsync();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionHandler service = new SubmissionHandler(database, storage, ".");
            await service.HandleNextMessage();

            Assert.True(service.ShouldSleep);
        }

        [Trait("UsesDocker", "Yes")]
        [Theory]
        [InlineData(0, 10)]
        [InlineData(1, 9)]
        public async Task TestJudgingServiceCanRunSumbissions(int submissionIndex, int score)
        {
            MockedCompetitionDatabase database = await MakeCompetitionDatabaseAsync();
            MockedAzureStorageClient storage = MakeAzureStorage();

            SubmissionHandler service = new SubmissionHandler(database, storage, ".");
            await storage.AddSubmissionToQueue(GetSubmissionGuids()[submissionIndex], GetCompetitionGuid(), new Guid());
            await service.HandleNextMessage();

            Submission submission = await database.GetSubmission(GetSubmissionGuids()[submissionIndex]);
            output.WriteLine(submission.Tests.ToString());
            Assert.Equal(SubmissionStatus.Completed, submission.Status);
            Assert.Equal(score, submission.Score);

            Assert.False(service.ShouldSleep);
        }

        [Trait("UsesDocker", "Yes")]
        [Fact]
        public async Task TestJudingServiceCanHandleSubmissionThatYieldsNoJsonFile()
        {
            await TestJudgingServiceCanRunSumbissions(2, 0);
        }
    }
}
