﻿using Commons.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tests.Mocking;

namespace Tests
{
    public abstract class BaseJudgingServiceFixture
    {
        public BaseJudgingServiceFixture()
        {
            SampleZipper.ZipSamples();
        }

        public Guid GetCompetitionGuid()
        {
            return new Guid("212328eb-9166-4ed2-a0ec-0c08d7aac276");
        }

        public Guid[] GetSubmissionGuids()
        {
            return new Guid[]
            {
                new Guid("da20349e-dc2a-4b2e-8dfe-8edb52fdb14f"),
                new Guid("dccf23c5-3343-4b21-8c83-6273536acaa5"),
                new Guid("f0929df5-2093-44cf-b48d-78de5be3056e")
            };
        }

        protected async Task<MockedCompetitionDatabase> MakeCompetitionDatabaseAsync()
        {
            var database = new MockedCompetitionDatabase();
            await database.AddCompetition(new Competition
            {
                Id = GetCompetitionGuid(),
                Deadline = DateTime.Now.AddDays(2),
                Description = "Description",
                Title = "Name",
                EmbedUrl = "Url"
            });

            Guid[] submissionIds = GetSubmissionGuids();
            foreach (Guid guid in submissionIds)
            {
                await database.AddSubmission(new Submission
                {
                    Id = guid,
                    CompetitionId = GetCompetitionGuid(),
                    Status = SubmissionStatus.Opened,
                    Submitted = DateTime.Now.AddHours(-1),
                    UserId = new Guid()
                });
            }

            return database;
        }

        protected MockedAzureStorageClient MakeAzureStorage()
        {
            return new MockedAzureStorageClient();
        }
    }
}
