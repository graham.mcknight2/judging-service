﻿using Commons;
using Commons.Auth;
using Commons.Models;
using Commons.Models.Auth;
using CompetitionService.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Tests.Mocking;
using Xunit;

namespace Tests
{
    public class LoginControllerTester
    {

        [Fact]
        public async Task CanLogInToCreatedAccount()
        {
            string email = "hello@example.com";
            string password = "password";

            var database = new MockedCompetitionDatabase();
            var loginController = new LoginController(database);

            await loginController.NewUser(new LoginPair
            {
                Email = email,
                Password = password
            });

            User user = await database.GetUser(email);

            var encrypter = new PasswordEncrypter(Secrets.PasswordKey, Secrets.PasswordIV);
            
            IActionResult result = await loginController.Login(new LoginPair
            {
                Email = email,
                Password = password
            });

           Assert.IsType<ContentResult>(result);
        }

        [Fact]
        public async Task LoginRejectsIncorrectPassword()
        {
            string email = "hello@example.com";
            string password = "password";

            var database = new MockedCompetitionDatabase();
            var loginController = new LoginController(database);

            await loginController.NewUser(new LoginPair
            {
                Email = email,
                Password = password
            });
            
            IActionResult result = await loginController.Login(new LoginPair
            {
                Email = email,
                Password = "wrongPassword"
            });

            Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public async Task CanCreateNewUserAccount()
        {
            string email = "hello@example.com";

            var database = new MockedCompetitionDatabase();
            var controller = new LoginController(database);

            IActionResult result = await controller.NewUser(new LoginPair
            {
                Email = email,
                Password = "password"
            });

            var createdResult = Assert.IsType<CreatedResult>(result);
            Assert.Equal(201, createdResult.StatusCode);

            var user = Assert.IsType<User>(createdResult.Value);
            Assert.Equal(email, user.Email);

            Assert.True(await database.ExistsUser(email));
        }

        [Fact]
        public async Task CannotCreateAccountWithSameEmail()
        {
            string email = "hello@example.com";

            var database = new MockedCompetitionDatabase();
            var controller = new LoginController(database);

            await controller.NewUser(new LoginPair
            {
                Email = email,
                Password = "password"
            });

            IActionResult result = await controller.NewUser(new LoginPair
            {
                Email = email,
                Password = "otherPassword"
            });

            var statusCodeResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(409, statusCodeResult.StatusCode);
        }
    }
}
