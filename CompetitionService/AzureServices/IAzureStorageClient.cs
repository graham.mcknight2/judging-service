﻿using Commons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureServices
{
    public interface IAzureStorageClient
    {
        Task<string> AuthorizeSubmissionUpload(Guid id);
        Task<string> AuthorizeContestUpload(Guid id);

        Task AddSubmissionToQueue(Guid submissionId, Guid contestId, Guid user);
        Task<SubmissionMessage> NextMessage();
        Task DownloadScript(Guid id, string location);
        Task DownloadSubmission(Guid id, string location);
    }
}
