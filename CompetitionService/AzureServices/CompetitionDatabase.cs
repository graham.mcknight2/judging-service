﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons.Models;
using Microsoft.EntityFrameworkCore;

namespace AzureServices
{
    public class CompetitionDatabase : DbContext, ICompetitionDatabase
    {
        public CompetitionDatabase(DbContextOptions<CompetitionDatabase> options) : base(options)
        {

        }

        public DbSet<Competition> Competitions { get; set; }
        public DbSet<Submission> Submissions { get; set; }
        public DbSet<User> Users { get; set; }

        public void Initialize()
        {
            Database.EnsureCreated();
        }

        public async Task<Competition> GetCompetition(Guid id)
        {
            return await Competitions.FindAsync(id);
        }

        public async Task<Submission> GetSubmission(Guid id)
        {
            return await Submissions.FindAsync(id);
        }

        public async Task AddSubmission(Submission submission)
        {
            await Submissions.AddAsync(submission);
        }

        public async Task AddCompetition(Competition competition)
        {
            await Competitions.AddAsync(competition);
        }

        public async Task<ICollection<Competition>> CompetitionsAfterDate(DateTime date)
        {
            return await (from b in Competitions where b.Deadline > date select b).ToListAsync();
        }

        public async Task<ICollection<Submission>> SubmissionsForUser(Guid id)
        {
            return await (from b in Submissions where b.UserId == id select b).ToListAsync();
        }

        public async Task<User> GetUser(Guid id)
        {
            return await Users.FindAsync(id);
        }

        public async Task<User> GetUser(string email)
        {
            return await Users.FirstOrDefaultAsync(user => user.Email == email);
        }

        public async Task<bool> ExistsUser(string email)
        {
            return await Users.AnyAsync(user => user.Email == email);
        }

        public Task UpdateUser(User user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateSubmission(Submission submission)
        {
            throw new NotImplementedException();
        }

        public Task UpdateCompetition(Competition competition)
        {
            throw new NotImplementedException();
        }

        public Task AddUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
