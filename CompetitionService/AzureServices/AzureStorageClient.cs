﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;
using Commons.Models;
using System.IO;

namespace AzureServices
{
    public class AzureStorageClient: IAzureStorageClient
    {
        private CloudStorageAccount account;
        private CloudQueueClient queueClient;
        private CloudBlobClient blobClient;

        public AzureStorageClient(string connectionString)
        {
            account = CloudStorageAccount.Parse(connectionString);
            queueClient = account.CreateCloudQueueClient();
            blobClient = account.CreateCloudBlobClient();
        }

        public async Task Initialize()
        {
            // Add CORS rules to allow uploads from any client
            // TODO: Specify certain rules once known
            ServiceProperties properties = await blobClient.GetServicePropertiesAsync();
            properties.Cors = new CorsProperties();
            
            // Taken from https://blogs.msdn.microsoft.com/windowsazurestorage/2014/02/03/windows-azure-storage-introducing-cors/
            properties.Cors.CorsRules.Add(new CorsRule
            {
                AllowedHeaders = new List<string>() { "*" },
                AllowedMethods = CorsHttpMethods.Put | CorsHttpMethods.Get | CorsHttpMethods.Head | CorsHttpMethods.Post,
                AllowedOrigins = new List<string>() { "*" },
                ExposedHeaders = new List<string>() { "*" }
            });

            await blobClient.SetServicePropertiesAsync(properties);
        }

        public async Task AddSubmissionToQueue(Guid submissionId, Guid contestId, Guid user)
        {
            CloudQueue queue = queueClient.GetQueueReference("submission-queue");

            await queue.CreateIfNotExistsAsync();

            CloudQueueMessage message = new CloudQueueMessage(
                JsonConvert.SerializeObject(
                new SubmissionMessage()
                {
                    Submission = submissionId,
                    Competition = contestId,
                    User = user
                }));

            await queue.AddMessageAsync(message);
        }

        public async Task<SubmissionMessage> NextMessage()
        {
            CloudQueue queue = queueClient.GetQueueReference("submission-queue");
            CloudQueueMessage message = await queue.GetMessageAsync();
            if (message == null)
            {
                return null;
            }

            var submissionMessage = JsonConvert.DeserializeObject<SubmissionMessage>(message.AsString);
            await queue.DeleteMessageAsync(message);
            return submissionMessage;

        }

        public async Task<string> AuthorizeContestUpload(Guid id)
        {
            return await authorizeUploadInContainer("contests", id);
        }

        public async Task<string> AuthorizeSubmissionUpload(Guid id)
        {
            return await authorizeUploadInContainer("submissions", id);
        }

        private async Task<string> authorizeUploadInContainer(string containerName, Guid id)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync();

            CloudBlockBlob blob = container.GetBlockBlobReference(id.ToString("D"));

            SharedAccessBlobPolicy policy = new SharedAccessBlobPolicy()
            {
                SharedAccessExpiryTime = DateTime.Now.AddYears(3),
                Permissions = SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Create
            };

            string sasToken = blob.GetSharedAccessSignature(policy);

            return blob.Uri + sasToken;
        }

        public async Task DownloadScript(Guid id, string location)
        {
            await download("contests", id, location);
        }

        public async Task DownloadSubmission(Guid id, string location)
        {
            await download("submissions", id, location);
        }

        private async Task download(string containerName, Guid id, string location)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            await container.CreateIfNotExistsAsync();

            CloudBlockBlob blob = container.GetBlockBlobReference(id.ToString("D"));
            await blob.DownloadToFileAsync(location, System.IO.FileMode.Create);
        }
    }
}
