﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Commons.Models;

namespace AzureServices
{
    public interface ICompetitionDatabase
    {
        Task<Submission> GetSubmission(Guid id);
        Task<Competition> GetCompetition(Guid id);

        Task AddSubmission(Submission submission);
        Task AddCompetition(Competition competition);
        Task AddUser(User user);

        Task<ICollection<Competition>> CompetitionsAfterDate(DateTime date);
        Task<ICollection<Submission>> SubmissionsForUser(Guid id);

        Task<User> GetUser(Guid id);
        Task<User> GetUser(string email);
        Task<bool> ExistsUser(string email);

        Task UpdateUser(User user);
        Task UpdateSubmission(Submission submission);
        Task UpdateCompetition(Competition competition);
    }
}
