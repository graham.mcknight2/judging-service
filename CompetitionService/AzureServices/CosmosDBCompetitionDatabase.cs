﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Commons.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Configuration;
using Newtonsoft;

namespace AzureServices
{
    public class CosmosDBCompetitionDatabase : ICompetitionDatabase
    {
        private static readonly string DatabaseName = "CompetitionDB";
        private static readonly string CompetitionCollectionName = "Competitions";
        private static readonly string SubmissionCollectionName = "Submissions";
        private static readonly string UserCollectionName = "Users";
        private static readonly string ConfigEndpointName = "CosmosDBEndpoint";
        private static readonly string ConfigKeyName = "CosmosDBKey";

        DocumentClient client;

        public CosmosDBCompetitionDatabase(IConfiguration config)
        {
            client = new DocumentClient(new Uri(config[ConfigEndpointName]), config[ConfigKeyName]);
        }

        public async Task Initialize()
        {
            await client.CreateDatabaseIfNotExistsAsync(new Database { Id = DatabaseName });

            var competitionCollection = new DocumentCollection { Id = CompetitionCollectionName };

            // Deadlines need to be indexed by range for dates so we can have upcoming competitions 
            competitionCollection.IndexingPolicy.IncludedPaths.Add(
                new IncludedPath()
                {
                    Path = "/deadline/?",
                    Indexes = new Collection<Index>
                    {
                        new RangeIndex(DataType.String) { Precision = -1 }
                    }
                });

            competitionCollection.IndexingPolicy.IncludedPaths.Add(
                new IncludedPath()
                {
                    Path = "/*",
                    Indexes = new Collection<Index>
                    {
                        new RangeIndex(DataType.String) { Precision = -1 },
                        new RangeIndex(DataType.Number) { Precision = -1 }
                    }
                });

            await client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(DatabaseName), competitionCollection);

            await client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(DatabaseName),
                new DocumentCollection { Id = SubmissionCollectionName });

            await client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(DatabaseName),
                new DocumentCollection { Id = UserCollectionName });
        }

        public async Task AddCompetition(Competition competition)
        {
            var resposne = await client.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, CompetitionCollectionName),
                competition);
        }

        public async Task AddSubmission(Submission submission)
        {
            var response = await client.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, SubmissionCollectionName),
                submission);
        }

        public async Task<Competition> GetCompetition(Guid id)
        {
            try
            {
                return await client.ReadDocumentAsync<Competition>(
                    UriFactory.CreateDocumentUri(DatabaseName, CompetitionCollectionName, id.ToString()));
               
            }
            catch (DocumentClientException)
            {
                // In the case of a 404 we have nothing to return.
                // TODO: Handle 429 by logging
                return null;
            }
        }

        public async Task<Submission> GetSubmission(Guid id)
        {
            try
            {
                return await client.ReadDocumentAsync<Submission>(
                    UriFactory.CreateDocumentUri(DatabaseName, SubmissionCollectionName, id.ToString()));
            }
            catch (DocumentClientException)
            {
                // TODO: Handle 429 by logging
                return null;
            }
        }

        public async Task<ICollection<Submission>> SubmissionsForUser(Guid id)
        {
            var submissions = client.CreateDocumentQuery<Submission>(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, SubmissionCollectionName))
                .Where(b => b.UserId == id)
                .AsDocumentQuery();

            return await collectAll(submissions);
        }

        public async Task<ICollection<Competition>> CompetitionsAfterDate(DateTime date)
        {
            var competition = client.CreateDocumentQuery<Competition>(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, CompetitionCollectionName))
                .Where(b => b.Deadline > date)
                .AsDocumentQuery();

            return await collectAll(competition);
        }

        public async Task<Commons.Models.User> GetUser(Guid id)
        {
            try
            {
                return await client.ReadDocumentAsync<Commons.Models.User>(
                    UriFactory.CreateDocumentUri(DatabaseName, UserCollectionName, id.ToString()));
            }
            catch (DocumentClientException)
            {
                // TODO: Handle 429 by logging
                return null;
            }
        }

        public async Task<Commons.Models.User> GetUser(string email)
        {
            var user = client.CreateDocumentQuery<Commons.Models.User>(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, UserCollectionName))
                .Where(b => b.Email == email)
                .AsDocumentQuery();

            return (await user.ExecuteNextAsync<Commons.Models.User>()).FirstOrDefault();
        }

        public async Task<bool> ExistsUser(string email)
        {
            int result = await client.CreateDocumentQuery<Commons.Models.User>(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, UserCollectionName))
                .Where(b => b.Email == email)
                .CountAsync();

            // We should only have one account with the email, but we should check just in case
            return result >= 1;
        }

        public async Task UpdateUser(Commons.Models.User user)
        {
            try
            {
                await client.UpsertDocumentAsync(
                    UriFactory.CreateDocumentCollectionUri(DatabaseName, UserCollectionName),
                    user);
            }
            catch (DocumentClientException e)
            {
                // TODO: Handle by logging or notifying the user
                Console.WriteLine("Got an exception while trying to update user " + e.Message);
            }
        }

        public async Task UpdateSubmission(Submission submission)
        {
            try
            {
                await client.UpsertDocumentAsync(
                    UriFactory.CreateDocumentCollectionUri(DatabaseName, SubmissionCollectionName),
                    submission);
            }
            catch (DocumentClientException e)
            {
                // TODO: Handle by logging or notifying the user
                Console.WriteLine("Got an exception while trying to update submission " + e.Message);
            }
        }

        public async Task UpdateCompetition(Competition competition)
        {
            try
            {
                await client.UpsertDocumentAsync(
                    UriFactory.CreateDocumentCollectionUri(DatabaseName, CompetitionCollectionName),
                    competition);
            }
            catch (DocumentClientException e)
            {
                // TODO: Handle by logging or notifying the user
                Console.WriteLine("Got an exception while trying to update competition " + e.Message);
            }
        }

        private async Task<ICollection<T>> collectAll<T>(IDocumentQuery<T> query)
        {
            List<T> results = new List<T>();
            while (query.HasMoreResults)
            {
                var page = await query.ExecuteNextAsync<T>();
                results.AddRange(page);
            }
            return results;
        }

        public async Task AddUser(Commons.Models.User user)
        {
            var resposne = await client.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(DatabaseName, UserCollectionName),
                user);
        }
    }
}
