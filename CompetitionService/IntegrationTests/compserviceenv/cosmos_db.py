import pydocumentdb
import pydocumentdb.document_client as document_client
import compserviceenv.constants as constants
import json

class CosmosDB(object):
    """
    Handler for CosmosDB to perform queries on stored data.
    """
    def __init__(self):
        self.client = document_client.DocumentClient(
            constants.COSMOS_DB_ENDPOINT, {'masterKey': constants.COSMOS_DB_KEY})

        self.db_link = 'dbs/' + constants.COSMOS_DB_NAME
        self.user_link = self.db_link + '/colls/Users'
        self.submission_link = self.db_link + '/colls/Submissions'
        self.competition_link = self.db_link + '/colls/Competitions'

    def clear_data(self):
        users = list(self.client.ReadDocuments(self.user_link))
        for user in users:
            self.client.DeleteDocument(self.user_link + '/docs/' + user['id'])

        submissisions = list(self.client.ReadDocuments(self.submission_link))
        for submission in submissisions:
            self.client.DeleteDocument(self.submission_link + '/docs/' + submission['id'])
        
        competitions = list(self.client.ReadDocuments(self.competition_link))
        for competition in competitions:
            self.client.DeleteDocument(self.competition_link + '/docs/' + competition['id'])


    def load_data(self, data_filename):
        with open(data_filename) as data_file:
            data = json.load(data_file)
        users = data['users']
        submissions = data['submissions']
        competitions = data['competitions']
        for user in users:
            self.put_user(user)
        for submission in submissions:
            self.put_submission(submission)
        for competition in competitions:
            self.put_competition(competition)
   
    def get_user(self, id):
        doc = self.client.ReadReadDocument(self.user_link + '/docs/' + str(id))
        return doc

    def put_user(self, doc):
        self.client.UpsertDocument(self.user_link, doc)
    
    def get_submission(self, id):
        doc = self.client.ReadDocument(self.submission_link + '/docs/' + str(id))
        return doc

    def put_submission(self, doc):
        self.client.UpsertDocument(self.submission_link, doc)

    def get_competition(self, id):
        doc = self.client.ReadDocument(self.competition_link + '/docs/' + str(id))
        return doc

    def put_competition(self, doc):
        self.client.UpsertDocument(self.competition_link, doc)