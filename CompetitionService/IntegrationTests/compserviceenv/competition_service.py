import http.client
import json
import compserviceenv.constants as constants

class CompetitionService(object):
    """
    Handler that controls the competition service and makes requests to it.
    """
    def __init__(self):
        self.auth_token = ''
        pass

    def auth(self, username, password):
        self.auth_token = ''
        self.auth_token = self.make_request('POST', '/api/Login', 200, {'email': username, 'password': password})

    def logout(self):
        self.auth_token = ''

    def make_request(self, method, endpoint, expectedStatusCode, data = None):
        client = http.client.HTTPConnection(constants.COMPETITION_SERVICE_URL, timeout = 15)
        print('Making a rest request to endpoint: ' + constants.COMPETITION_SERVICE_URL + endpoint)

        header = {'Content-Type': 'application/json'}
        if self.auth_token != '':
            header['Token'] = self.auth_token

        if data == None:
            client.request(method, endpoint, headers=header)
        else:
            print('Data in request body: ' + json.dumps(data))
            client.request(method, endpoint, bytes(json.dumps(data), 'utf-8'), header)

        response = client.getresponse()
        if response.code != expectedStatusCode:
            client.close()
            raise Exception('Incorrect response code when submitting results: ' + str(response.code))
        
        response = response.read().decode('utf-8')
        client.close()
        print('Got the following data in response: ' + response)
        return response

    def submit_results(self, id, data):
        self.make_request('PUT', '/api/Submission/' + id, 201, data)
    
    def get_submission(self, id):
        return json.loads(self.make_request('GET', '/api/Submission/' + id, 200))

    def get_user_submissions(self, id):
        return json.loads(self.make_request('GET', '/api/Submission/user/' + id, 200));

    def get_upcoming_competitions(self):
        return json.loads(self.make_request('GET', '/api/Competition/upcoming', 200))
