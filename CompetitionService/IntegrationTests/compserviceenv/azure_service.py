import compserviceenv.constants as constants
from azure.storage.queue.queueservice import QueueService
from azure.storage.blob.blockblobservice import BlockBlobService

class AzureService(object):
    """Handles the queue and blob storage."""
    def __init__(self):
        self.queue = QueueService(connection_string=constants.AZURE_STORAGE_CONNECTION_STRING)
        self.blobs = BlockBlobService(connection_string=constants.AZURE_STORAGE_CONNECTION_STRING)

    def clear_all(self):
        self.queue.clear_messages('submission-queue')
        competitionBlobs = self.blobs.list_blobs('contests')
        for blob in competitionBlobs:
            self.blobs.delete_blob('contests', blob.name)
        submissionBlobs = self.blobs.list_blobs('submissions')
        for blob in submissionBlobs:
            self.blobs.delete_blob('submissions', blob.name)

    def add_queue_message(self, competitionId, submissionId, userId):
        self.queue.put_message('submission-queue', {'competition': competitionId, 'submission': submissionId, 'user': userId})

    def upload_submission_file(self, id, path):
        self.blobs.create_blob_from_path('submissions', id, path)

    def upload_competition_file(self, id, path):
        self.blobs.create_blob_from_path('contests', id, path)
