import unittest
import compserviceenv.competition_service as competition_service
import compserviceenv.cosmos_db as cosmos_db


class Test_UpcomingCompetitionsIT(unittest.TestCase):

    def setUp(self):
        self.service = competition_service.CompetitionService()
        self.db = cosmos_db.CosmosDB()
        self.db.clear_data()
        self.db.load_data('sample_data/data_1.json')
        self.service.auth('gfm13friday@gmail.com', 'password123')

    def test_upcoming_competitions(self):
        competitions = self.service.get_upcoming_competitions()
        self.assertEqual(len(competitions), 1)
        self.assertEqual(competitions[0]['id'], '3692918e-2157-4310-a6ee-754214890491')

if __name__ == '__main__':
    unittest.main()
