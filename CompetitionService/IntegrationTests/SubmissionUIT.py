import unittest
import time
import os
from compserviceenv import constants
from compserviceenv import cosmos_db
from compserviceenv import azure_service
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class Test_SubmissionUIT(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.db = cosmos_db.CosmosDB()
        self.db.clear_data()
        self.db.load_data('sample_data/data_2.json')
        self.storage = azure_service.AzureService()
        self.storage.clear_all()
        self.samplesDirectory = os.path.abspath(os.path.join(constants.PROJECT_ROOT + 'samples'))
        # This competition is the one we are uploading to in this test
        self.storage.upload_competition_file('3692918e-2157-4310-a6ee-754214890491',
                os.path.join(self.samplesDirectory, 'basic-competition', '212328eb-9166-4ed2-a0ec-0c08d7aac276.zip'))
        self.driver.get(constants.COMPETITION_SERVICE_URL_EASY)
        self.driver.implicitly_wait(10)

    def login(self):
        self.driver.find_element_by_link_text('Login').click()
        
        email_elem = self.driver.find_element_by_id('login_email_input')
        email_elem.send_keys('gfm13friday@gmail.com')
        
        password_elem = self.driver.find_element_by_id('login_password_input')
        password_elem.send_keys('password123')

        self.driver.find_element_by_id('login_submit_button').click()
        time.sleep(2)

    def test_can_upload_submission(self):
        self.login()
        self.driver.find_element_by_link_text('Competitions').click()
        
        # Competition browser page
        collapse = self.driver.find_element_by_xpath('//*[@id="3692918e-2157-4310-a6ee-754214890491"]//*[@id="competition_title"]')
        collapse.click()
        time.sleep(2)
        self.driver.find_element_by_link_text('View Competition Page').click()
        time.sleep(2)

        # Actual competition page
        self.driver.find_element_by_link_text('Submit').click()
        uploads_input = self.driver.find_element_by_id('uploads_input')
        submissionFilepath = os.path.join(self.samplesDirectory, 'basic-competition', 'da20349e-dc2a-4b2e-8dfe-8edb52fdb14f.zip')
        uploads_input.send_keys(submissionFilepath)
        self.driver.find_element_by_id('submission_upload_button').click()
        time.sleep(2)

        # Submissions view
        self.driver.find_element_by_xpath('//*[@id="3692918e-2157-4310-a6ee-754214890491"]//*[@id="competition_title"]').click()
        # Implicit check if this status element exists, since it will disappear
        # when the submission gets processed
        status_elem = self.driver.find_element_by_xpath('//*[@id="3692918e-2157-4310-a6ee-754214890491"]//*[@id="submission_status"]')
        self.assertEqual(status_elem.text, 'Queued')

        # Give the service some time to process the submission, and then check it
        # This will also be a useful test when caching is implemented to make sure
        # that no processed submission is left cached as queued
        time.sleep(30)
        self.driver.find_element_by_link_text('Competitions').click()
        time.sleep(2)
        self.driver.find_element_by_link_text('Submissions').click()

        # TODO: Check that it has been processed

if __name__ == '__main__':
    unittest.main()
