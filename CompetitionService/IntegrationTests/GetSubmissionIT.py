import unittest
import compserviceenv.competition_service as competition_service
import compserviceenv.cosmos_db as cosmos_db

class Test_GetSubmissionIT(unittest.TestCase):

    def setUp(self):
        self.service = competition_service.CompetitionService()
        self.db = cosmos_db.CosmosDB()
        self.db.clear_data()
        self.db.load_data('sample_data/data_1.json')
        self.service.auth('gfm13friday@gmail.com', 'password123')

    def test_can_get_submission(self):
        doc = self.service.get_submission('c53f54b6-e903-40a9-8f14-0cc1d88dcc40')

        self.assertEqual(doc['id'], 'c53f54b6-e903-40a9-8f14-0cc1d88dcc40')
        self.assertEqual(doc['userId'], 'f4baf873-a3d6-4b61-b36e-3727ae2da017')
        self.assertEqual(doc['competitionId'], '3692918e-2157-4310-a6ee-754214890491')
        self.assertEqual(doc['status'], 'Completed')
        self.assertEqual(doc['submitted'], '2018-09-14T21:47:14.0361483-07:00')
        self.assertEqual(doc['score'], 10)

    def test_can_get_submissions_for_user(self):
        submissions = self.service.get_user_submissions('f4baf873-a3d6-4b61-b36e-3727ae2da017')
        self.assertEqual(len(submissions), 2)
        self.assertEqual(submissions[0]['userId'], 'f4baf873-a3d6-4b61-b36e-3727ae2da017')
        self.assertEqual(submissions[1]['userId'], 'f4baf873-a3d6-4b61-b36e-3727ae2da017')

if __name__ == '__main__':
    unittest.main()
