﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Commons.Auth
{
    public class PasswordEncrypter
    {

        public string Key { get; private set; }
        public string IV { get; private set; }

        public PasswordEncrypter(string key, string iv)
        {
            Key = key;
            IV = iv;
        }

        public string Decrypt(string information)
        {
            string decryptedInfo;

            // Use Rijndael for symmetric-key decryption of password information
            using (RijndaelManaged rijndael = new RijndaelManaged())
            {
                // The key and initialization vector are secrets known only to
                // our program in the case that someone steals our passwords
                rijndael.IV = Convert.FromBase64String(IV);
                rijndael.Key = Convert.FromBase64String(Key);

                ICryptoTransform transform = rijndael.CreateDecryptor();

                // We store the byte result as a base 64 string when encrypting
                // so we have to undo this tranformation first
                using (var memoryStream = new MemoryStream(Convert.FromBase64String(information)))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Read))
                    {
                        using (var reader = new StreamReader(cryptoStream))
                        {
                            decryptedInfo = reader.ReadToEnd();
                        }
                    }
                }
            }
            return decryptedInfo;
        }

        public string Encrypt(string information)
        {
            string encryptedInfo;
            using (RijndaelManaged rijndael = new RijndaelManaged())
            {
                // The IV and key are kept private to our application so only this
                // application can access it.
                rijndael.IV = Convert.FromBase64String(IV);
                rijndael.Key = Convert.FromBase64String(Key);

                ICryptoTransform transform = rijndael.CreateEncryptor();

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write))
                    {
                        using (var writer = new StreamWriter(cryptoStream))
                        {
                            writer.Write(information);
                        }
                        // The encrypted information is turned into a base 64 string so that
                        // it can be entered into the database.
                        encryptedInfo = Convert.ToBase64String(memoryStream.ToArray());
                    }
                }
            }

            return encryptedInfo;
        }
    }
}
