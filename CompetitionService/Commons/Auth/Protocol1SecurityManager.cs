﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Auth
{
    public class Protocol1SecurityManager : BasePasswordSecurityManager
    {
        private PasswordEncrypter encrypter;
        private PasswordHasher hasher;

        public Protocol1SecurityManager(PasswordEncrypter encrypter, PasswordHasher hasher)
        {
            this.encrypter = encrypter;
            this.hasher = hasher;
        }

        public override bool IsCorrectPassword(string plaintext, string encrypted)
        {
            string hash = encrypter.Decrypt(encrypted);
            return hash == hasher.Hash(plaintext, hasher.GetSalt(hash));
        }

        public override string EncryptPassword(string plaintext)
        {
            string hash = hasher.Hash(plaintext, null);
            return encrypter.Encrypt(hash);
        }
    }
}
