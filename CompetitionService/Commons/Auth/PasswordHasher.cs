﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Auth
{
    public class PasswordHasher
    {
        private static readonly string Delimiter = ":";

        public string Hash(string password, string salt)
        {
            if (salt == null)
            {
                salt = generateSalt();
            }

            return salt + Delimiter + protocol1Hash(password, salt);
        }

        public string GetSalt(string hashedPassword)
        {
            string[] tokens = hashedPassword.Split(Delimiter.ToCharArray());
            if (tokens.Length != 2)
            {
                throw new ArgumentException();
            }
            return tokens[0];
        }

        private string generateSalt()
        {
            byte[] salt = new byte[128];
            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(salt);
            }
            return Convert.ToBase64String(salt);
        }

        private string protocol1Hash(string password, string salt)
        {
            byte[] rawHash = KeyDerivation.Pbkdf2(
                password,
                Convert.FromBase64String(salt),
                KeyDerivationPrf.HMACSHA512,
                70000,
                512 / 8);

            return Convert.ToBase64String(rawHash);
        }
    }
}
