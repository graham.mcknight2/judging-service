﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Commons;

namespace Commons.Auth
{
    public abstract class BasePasswordSecurityManager
    {
        public static readonly int CurrentPasswordProtocolVersion = 1;

        public static BasePasswordSecurityManager GetPasswordSecurity(int protocol)
        {
            switch (protocol)
            {
                case 1:
                    return new Protocol1SecurityManager(
                        new PasswordEncrypter(Secrets.PasswordKey, Secrets.PasswordIV),
                        new PasswordHasher());
                default:
                    return null;
            }
        }

        public static BasePasswordSecurityManager GetCurrentPasswordSecurity()
        {
            return GetPasswordSecurity(CurrentPasswordProtocolVersion);
        }

        public abstract bool IsCorrectPassword(string plaintext, string encrypted);
        public abstract string EncryptPassword(string plaintext);
    }
}
