﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace Commons.Models
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SubmissionStatus
    {
        Opened, // Client has been given permission to upload to a blob
        Queued, // Client finished upload to blob
        Processing, // The judging server is processing the submission
        Completed, // The submission was successfully scored
        Rejected // For any errors that may have occurred
    }

    [Serializable]
    public class Submission
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("userId")]
        public Guid UserId { get; set; }

        [JsonRequired]
        [JsonProperty("competitionId")]
        public Guid CompetitionId { get; set; }

        [JsonProperty("status")]
        public SubmissionStatus Status { get; set; }

        [JsonProperty("submitted")]
        public DateTime Submitted { get; set; }

        [JsonProperty("tests")]
        public JToken Tests { get; set; }

        [JsonProperty("score")]
        public double Score { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
