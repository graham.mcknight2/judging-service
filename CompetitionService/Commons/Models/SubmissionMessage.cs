﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Models
{
    [Serializable]
    public class SubmissionMessage
    {
        [JsonProperty("submission")]
        public Guid Submission { get; set; }

        [JsonProperty("competition")]
        public Guid Competition { get; set; }

        [JsonProperty("user")]
        public Guid User { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
