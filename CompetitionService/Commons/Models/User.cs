﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Models
{
    /// <summary>
    /// Represents an account that a user can log in to with a username and
    /// password.
    /// </summary>
    [Serializable]
    public class User
    {
        // For strings, format with D format
        // https://msdn.microsoft.com/en-us/library/97af8hh4(v=vs.110).aspx
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("contributor")]
        public bool Contributor { get; set; }

        [JsonProperty("passwordHash")]
        public string PasswordHash { get; set; }
        [JsonProperty("passwordVersion")]
        public int PasswordVersion { get; set; }

        // TODO: Add information surrounding the permissions and access
        // that the user has.
    }
}
