﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Commons.Models
{
    public class Competition
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonRequired]
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("embedUrl")]
        public string EmbedUrl { get; set; }

        // TODO: Add when Vue client can pick and serialize a date.
        [JsonRequired]
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonExtensionData]
        private IDictionary<string, JToken> _additionalData;

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
