﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using System.Security.Cryptography;

namespace Commons.Models.Auth
{
    public class Token
    {
        private static readonly string Delimiter = ".";
        private static readonly int NumberOfJwtParts = 3;

        public TokenHeader Header { get; set; }
        public TokenBody Body { get; set; }

        public static bool IsWellFormedToken(string token)
        {
            string[] parts = token.Split(Delimiter.ToCharArray());

            if (parts.Length != NumberOfJwtParts)
            {
                return false;
            }

            return parts[2] == getSignature(parts[0], parts[1]);
        }

        public static Token Parse(string token)
        {
            if (!IsWellFormedToken(token))
            {
                return null;
            }

            string[] parts = token.Split(Delimiter.ToCharArray());

            string jsonHeader = Encoding.ASCII.GetString(Convert.FromBase64String(parts[0]));
            string jsonBody = Encoding.ASCII.GetString(Convert.FromBase64String(parts[1]));

            TokenHeader header = JsonConvert.DeserializeObject<TokenHeader>(jsonHeader);
            TokenBody body = JsonConvert.DeserializeObject<TokenBody>(jsonBody);

            return new Token
            {
                Header = header,
                Body = body
            };
        }

        public override string ToString()
        {
            string jsonHeader = JsonConvert.SerializeObject(Header);
            string jsonBody = JsonConvert.SerializeObject(Body);

            string base64Header = Convert.ToBase64String(Encoding.ASCII.GetBytes(jsonHeader));
            string base64Body = Convert.ToBase64String(Encoding.ASCII.GetBytes(jsonBody));

            return base64Header + Delimiter + base64Body + Delimiter + getSignature(base64Header, base64Body);
        }

        private static string getSignature(string header, string body)
        {
            string toHash = header + Delimiter + body + Delimiter + Secrets.JwtSecret;
            byte[] asBytes = Encoding.ASCII.GetBytes(toHash);

            SHA256 sha = SHA256.Create();
            byte[] rawHash = sha.ComputeHash(asBytes);
            return Convert.ToBase64String(rawHash);
        }
    }
}
