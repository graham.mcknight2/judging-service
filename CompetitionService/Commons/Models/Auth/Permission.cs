﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Models.Auth
{
    /// <summary>
    /// Indicates that a user has access to a certain resource or
    /// group of resources.
    /// </summary>
    [Serializable]
    public class Permission
    {
        Guid UserId { get; set; }

        // TODO: Redefine this when the permission system is
        // laid out in more detail.
        Guid ResourceId { get; set; }
    }
}
