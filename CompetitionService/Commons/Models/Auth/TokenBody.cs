﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Models.Auth
{
    /// <summary>
    /// Represents the information that can be unpacked from
    /// a valid JWT issued from this service.
    /// </summary>
    [Serializable]
    public class TokenBody
    {
        [JsonRequired]
        public Guid UserId;
        public ICollection<Permission> Permissions;
    }
}
