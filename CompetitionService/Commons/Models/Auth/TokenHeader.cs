﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Commons.Models.Auth
{
    [Serializable]
    public class TokenHeader
    {
        [JsonRequired]
        public DateTime Issued { get; set; }
        [JsonRequired]
        public DateTime Expires { get; set; }

        [JsonRequired]
        public string Issuer { get; set; }

        [JsonRequired]
        public string ClientIpAdress { get; set; }
    }
}
