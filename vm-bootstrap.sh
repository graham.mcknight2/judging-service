# To be run in the home direvtory of the vm.
cd ~

if [ ! -d judging-service ]; then
    git clone https://github.com/gfmcknight/judging-service.git
fi

cd judging-service
git add .
git stash
git stash drop

git fetch origin deploy
git checkout origin/deploy
git rev-parse origin/deploy > ../rev

cd CompetitionService/
dotnet restore
dotnet publish JudgingService -c Release out

if [ ! d /app ]; then
    mkdir /app
fi
cp -r out/* /app/

cd /app
mkdir secrets
cp ~/settings.json secrets/

NOUPDATE=1
while [ $NOUPDATE -eq 1 ]; do
    dotnet JudgingService.dll
    # The app eventually exits and decides to check for updates

    cd ~/judging-service
    git fetch origin/deploy
    git rev-parse origin/deploy > ../new-rev

    cd ..
    diff rev new-rev
    if [ ! $? -eq 0 ]; then
        NOUPDATE=0
    fi

    cd /app
done

echo "Pulling an update"

cd ~
rm -rf /app/*
rm rev
mv rev-new rev

cd judging-service
git checkout origin/deploy

cp vm-bootstrap.sh ./..
cd ..
exec ./vm-bootstrap.sh