# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=judging-service:build-env /app/CompetitionService/out .
ENTRYPOINT ["dotnet", "CompetitionService.dll"]