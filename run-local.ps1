$ROOT=Get-Location

# Competition Service
Set-Location $ROOT/CompetitionService/CompetitionService
$competition = Start-Process dotnet -ArgumentList "run" -PassThru

# Vue.js Frontend - Disabled for now because it's built into the competition service.
#Set-Location $ROOT/web-client-new
#$front_end = Start-Process npm -ArgumentList "run serve"  -PassThru

# Competition Service
Set-Location $ROOT/CompetitionService/JudgingService
$judging = Start-Process dotnet -ArgumentList "run" -PassThru

Set-Location $ROOT

Read-Host "Press enter to exit all processes"

# These must be done with taskkill because they spawn child processes
taskkill.exe /pid $competition.Id /t /f
taskkill.exe /pid $judging.Id /t /f